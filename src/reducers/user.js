import { CHANGE_USER_TYPE } from "../constants/ActionTypes";

const userTypeInitialState = {
  userType: "Normal"
};

const userTypeReducer = (state = userTypeInitialState, action) => {
  switch (action.type) {
    case CHANGE_USER_TYPE:
      return {
        ...state,
        userType: action.userType
      };
    default:
      return state;
  }
};

export default userTypeReducer;
