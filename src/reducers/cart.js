import { ADD_TO_CART, REMOVE_FROM_CART, DECREMENT_QTY, INCREMENT_QTY, CHECKOUT_SUCCESS } from "../constants/ActionTypes";

export default function cartReducer(
  state = {
    cart: []
  },
  action
) {
  console.log("Cart reducer", action);
  switch (action.type) {
    case ADD_TO_CART:
      const productId = action.product.id;
      if (state.cart.findIndex(product => product.id === productId) !== -1) {
        const cart = state.cart.reduce((cartAcc, product) => {
          if (product.id === productId) {
            cartAcc.push({
              ...product,
              qty: product.qty + action.qty,
              sum: product.sum + action.qty * (action.product.salePrice - product.salePrice * (product.discount / 100)),
              colourSelected: action.colour,
              sizeSelected: action.size.toString(),
              variation_id: action.variation_id
            }); // Increment qty
          } else {
            cartAcc.push(product);
          }
          return cartAcc;
        }, []);
        return { ...state, cart };
      } else {
        return {
          ...state,
          cart: [
            ...state.cart,
            {
              ...action.product,
              qty: action.qty,
              sum: action.qty * (action.product.salePrice - action.product.salePrice * (action.product.discount / 100)),
              colourSelected: action.colour.toString(),
              sizeSelected: action.size.toString()
            }
          ]
        };
      }

    case DECREMENT_QTY:
      if (state.cart.findIndex(product => product.id === action.productId) !== -1) {
        const cart = state.cart.reduce((cartAcc, product) => {
          if (product.id === action.productId && product.qty > 1) {
            cartAcc.push({
              ...product,
              qty: product.qty - 1,
              sum: product.sum - (product.salePrice - product.salePrice * (product.discount / 100))
            });
          } else {
            cartAcc.push(product);
          }
          return cartAcc;
        }, []);
        return { ...state, cart };
      }

    case INCREMENT_QTY:
      if (state.cart.findIndex(product => product.id === action.productId) !== -1) {
        const cart = state.cart.reduce((cartAcc, product) => {
          if (product.id === action.productId) {
            cartAcc.push({
              ...product,
              qty: product.qty + 1,
              sum: product.sum + (product.salePrice - product.salePrice * (product.discount / 100))
            });
          } else {
            cartAcc.push(product);
          }
          return cartAcc;
        }, []);
        return { ...state, cart };
      }

    case REMOVE_FROM_CART:
      console.log(action.product_id, state.cart);
      return {
        cart: state.cart.filter(item => item.id !== action.product_id.id)
      };
    case CHECKOUT_SUCCESS:
      return {
        cart: []
      };

    default:
  }
  return state;
}
