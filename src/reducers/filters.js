import * as types from "../constants/ActionTypes";

const filtersReducerDefaultState = {
  brand: [],
  value: 0,
  sortBy: "",
  department: []
};

const filtersReducer = (state = filtersReducerDefaultState, action) => {
  switch (action.type) {
    case types.FILTER_BRAND:
      return {
        ...state,
        brand: action.brand
      };
    case types.FILTER_COLOR:
      return {
        ...state,
        color: action.color
      };
    case types.FILTER_PRICE:
      return {
        ...state,
        value: action.value
      };
    case types.SORT_BY:
      return {
        ...state,
        sortBy: action.sort_by
      };
    case types.FILTER_DEPARTMENT:
      return {
        ...state,
        department: action.department
      };
    default:
      return state;
  }
};

export default filtersReducer;
