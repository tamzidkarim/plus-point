// Get Departmentwise products
export const getDepartments = (products, department) => {
  let product_items;
  product_items = products.filter(product => product.departmentSlug == department);
  return product_items;
};

// Get Unique Brands from Json Data
export const getBrands = (products, category) => {
  let product_items;
  product_items = products.filter(product => {
    if (product.departmentSlug == category) {
      return product;
    }
  });
  var uniqueBrands = [];
  product_items.map((product, index) => {
    if (product.tags) {
      product.tags.map(tag => {
        if (uniqueBrands.indexOf(tag) === -1) {
          uniqueBrands.push(tag);
        }
      });
    }
  });
  return uniqueBrands;
};

export const getBrandsSubdepartment = (products, subcategory) => {
  let product_items;
  product_items = products.filter(product => {
    if (product.subDepartmentSlug == subcategory) {
      return product;
    }
  });
  var uniqueBrands = [];
  product_items.map((product, index) => {
    if (product.tags) {
      product.tags.map(tag => {
        if (uniqueBrands.indexOf(tag) === -1) {
          uniqueBrands.push(tag);
        }
      });
    }
  });
  return uniqueBrands;
};

// Get Unique Colors from Json Data
export const getColorsSubdepartments = (products, category) => {
  let product_items;
  product_items = products.filter(product => {
    if (product.subDepartmentSlug == category) {
      return product;
    }
  });
  var uniqueColors = [];
  product_items.map((product, index) => {
    if (product.colors) {
      product.colors.map(color => {
        if (uniqueColors.indexOf(color) === -1) {
          uniqueColors.push(color);
        }
      });
    }
  });

  return uniqueColors;
};

// Get Unique Colors from Json Data
export const getColors = (products, category) => {
  let product_items;
  product_items = products.filter(product => {
    if (product.departmentSlug == category) {
      return product;
    }
  });
  var uniqueColors = [];
  product_items.map((product, index) => {
    if (product.colors) {
      product.colors.map(color => {
        if (uniqueColors.indexOf(color) === -1) {
          uniqueColors.push(color);
        }
      });
    }
  });

  return uniqueColors;
};

// Get Minimum and Maximum Prices from Json Data
export const getMinMaxPriceSubdepartments = (products, category) => {
  let min = 100,
    max = 1000;

  let product_items;
  product_items = products.filter(product => {
    if (product.subDepartmentSlug == category) {
      return product;
    }
  });

  product_items.map((product, index) => {
    let v = product.price;
    min = v < min ? v : min;
    max = v > max ? v : max;
  });

  return { min: min, max: max };
};

// Get Minimum and Maximum Prices from Json Data
export const getMinMaxPrice = (products, category) => {
  // let min = 100,
  //   max = 1000;

  var max = Math.max.apply(
    Math,
    products.map(function(product) {
      return product.price;
    })
  );
  var min = Math.min.apply(
    Math,
    products.map(function(product) {
      return product.price;
    })
  );
  // let product_items;
  // product_items = products.filter(product => {
  //   if (product.departmentSlug == category) {
  //     return product;
  //   }
  // });

  // product_items.map((product, index) => {
  //   let v = product.price;
  //   min = v < min ? v : min;
  //   max = v > max ? v : max;
  // });

  return { min: min, max: max };
};

export const getMatchProduct = (data, { brand, color, value, sortBy, department }, category) => {
  // console.log("getMatch: ", data.products, category, department[0]);
  if (department == "all") {
    return data.products;
  }
  let filteredByDepartment;
  filteredByDepartment = data.products.filter(product => {
    if (department.includes(product.departmentSlug) || department.includes(product.subDepartmentSlug)) {
      return product;
    }
  });
  let result = filteredByDepartment;

  let filteredByBrand = [];
  if (brand.length > 0) {
    filteredByBrand = filteredByDepartment.filter(product => {
      return product.tags.some(item => brand.includes(item));
    });
    result = filteredByBrand;
  }

  let filteredByPrice = [];
  if (value != -1 && value != "none") {
    filteredByPrice = result.filter(product => {
      return product.price <= value;
    });
    result = filteredByPrice;
  }

  return result;
};

export const getMatchProductSubdepartments = (data, { brand, color, value, sortBy }, subcategory) => {
  let product_items;
  product_items = data.products.filter(product => {
    if (product.subDepartmentSlug == subcategory) {
      return product;
    }
  });
  return product_items;
};

//getVisibleproductsSubdepartments
export const getVisibleproductsSubdepartments = (data, { brand, color, value, sortBy }, subcategory) => {
  let product_items;

  product_items = data.products.filter(product => {
    if (product.subDepartmentSlug == subcategory) {
      return product;
    }
  });

  return product_items
    .filter(product => {
      let brandMatch;
      if (product.tags) brandMatch = product.tags.some(tag => brand.includes(tag));
      else brandMatch = true;

      let colorMatch;
      if (color && product.colors) {
        colorMatch = product.colors.includes(color);
      } else {
        colorMatch = true;
      }

      const startPriceMatch = typeof value.min !== "number" || value.min <= product.price;
      const endPriceMatch = typeof value.max !== "number" || product.price <= value.max;

      return brandMatch && colorMatch && startPriceMatch && endPriceMatch;
    })
    .sort((product1, product2) => {
      if (sortBy === "HighToLow") {
        return product2.price < product1.price ? -1 : 1;
      } else if (sortBy === "LowToHigh") {
        return product2.price > product1.price ? -1 : 1;
      } else if (sortBy === "Newest") {
        return product2.id < product1.id ? -1 : 1;
      } else if (sortBy === "AscOrder") {
        return product1.name.localeCompare(product2.name);
      } else if (sortBy === "DescOrder") {
        return product2.name.localeCompare(product1.name);
      } else {
        return product2.id > product1.id ? -1 : 1;
      }
    });
};

export const getVisibleproducts = (data, { brand, color, value, sortBy }, category) => {
  let product_items;

  product_items = data.products.filter(product => {
    if (product.departmentSlug == category) {
      return product;
    }
  });

  return product_items
    .filter(product => {
      let brandMatch;
      if (product.tags) brandMatch = product.tags.some(tag => brand.includes(tag));
      else brandMatch = true;

      let colorMatch;
      if (color && product.colors) {
        colorMatch = product.colors.includes(color);
      } else {
        colorMatch = true;
      }

      const startPriceMatch = typeof value.min !== "number" || value.min <= product.price;
      const endPriceMatch = typeof value.max !== "number" || product.price <= value.max;

      return brandMatch && colorMatch && startPriceMatch && endPriceMatch;
    })
    .sort((product1, product2) => {
      if (sortBy === "HighToLow") {
        return product2.price < product1.price ? -1 : 1;
      } else if (sortBy === "LowToHigh") {
        return product2.price > product1.price ? -1 : 1;
      } else if (sortBy === "Newest") {
        return product2.id < product1.id ? -1 : 1;
      } else if (sortBy === "AscOrder") {
        return product1.name.localeCompare(product2.name);
      } else if (sortBy === "DescOrder") {
        return product2.name.localeCompare(product1.name);
      } else {
        return product2.id > product1.id ? -1 : 1;
      }
    });
};

export const getCartTotal = cartItems => {
  // var total = 0;
  // for(var i=0; i<cartItems.length; i++){
  //     total += parseInt(cartItems[i].qty, 10)*parseInt((cartItems[i].price*cartItems[i].discount/100), 10);
  // }
  // return total;
  const total = cartItems.reduce((sum, item) => {
    return sum + item.sum;
  }, 0);

  return total;
};

export const getTotalDiscount = cartItems => {
  const totalDiscount = cartItems.reduce((sum, item) => {
    return sum + item.price * (item.discount / 100);
  }, 0);

  return totalDiscount;
};

// Get Trending Tag wise Collection
export const getTrendingTagCollection = (products, type, tag) => {
  const items = products.filter(product => {
    return product.category === type && product.tags.includes(tag);
  });
  return items.slice(0, 8);
};

// Get Trending Collection
export const getTrendingCollection = (products, type) => {
  const items = products.filter(product => {
    return product.category === type;
  });
  return items.slice(0, 8);
};

// Get Special 5 Collection
export const getSpecialCollection = (products, type) => {
  const items = products.filter(product => {
    return product.category === type;
  });
  return items.slice(0, 5);
};

// Get TOP Collection
export const getTopCollection = products => {
  const items = products.filter(product => {
    return product.rating > 4;
  });
  return items.slice(0, 8);
};

// Get New Products
export const getNewProducts = (products, type) => {
  const items = products.filter(product => {
    return product.new === true && product.category === type;
  });

  return items.slice(0, 8);
};

// Get Related Items
export const getRelatedItems = (products, type) => {
  const items = products.filter(product => {
    return product.category === type;
  });

  return items.slice(0, 4);
};

// Get Best Seller Furniture
export const getBestSellerProducts = (products, type) => {
  const items = products.filter(product => {
    return product.sale === true && product.category === type;
  });

  return items.slice(0, 8);
};

// Get Best Seller
export const getBestSeller = (products, category) => {
  // let product_items;
  // if (category=='shop'){
  //     product_items=products;
  // }else {
  //     product_items = products.filter(product => {
  //         for(var i = 0; i <  product.main_category.length; i++) {
  //             if (product.main_category[i]== category){
  //                 return product;
  //             }
  //         }
  //     });
  // }

  // const items = product_items.filter(product => {
  //     return product.sale === true;
  // })
  return products.slice(0, 8);
};

//getBestSellerNewProductDetails
export const getBestSellerNewProductDetails = products => {
  const items = products.filter(product => {
    return product.sale === true;
  });

  return items.slice(0, 8);
};

// Get Mens Wear
export const getMensWear = products => {
  const items = products.filter(product => {
    return product.isFeatured === 2;
  });

  return items.slice(0, 8);
};

// Get Womens Wear
export const getWomensWear = products => {
  const items = products.filter(product => {
    return product.sale == true;
  });

  return items.slice(0, 8);
};

// Get Single Product
export const getSingleItem = (products, id) => {
  const items = products.find(element => {
    return element.id === id;
  });
  return items;
};

const loginUser = userObj => ({
  type: "LOGIN_USER",
  payload: userObj
});

const initialState = {
  currentUser: {}
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case "LOGIN_USER":
      return { state, currentUser: action.payload };
    default:
      return state;
  }
}
