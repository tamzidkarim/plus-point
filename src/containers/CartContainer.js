import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import CartPage from "../components/common/headers/common/cart-header";
import { removeFromCart } from "../actions";
import { getCartTotal } from "../services";

class CartContainer extends Component {
  render() {
    const { cartList, total, symbol, removeFromCart, language } = this.props;
    return (
      <div>
        <ul>
          <li className="onhover-div mobile-cart" style={{ paddingLeft: "0" }}>
            <button onClick={addClassName}>
              <img src={`${process.env.PUBLIC_URL}/assets/images/cart-icon.png`} className="img-fluid" alt="" />
              <i className="fa fa-shopping-cart"></i>
              <div className="cart-qty-cls">
                {language === "en" ? "Bag" : "ব্যাগ"} ({cartList.length})
              </div>
            </button>

            <ul id="test" className="show-div shopping-cart">
              <div style={{ marginBottom: "40px" }}>
                <h5 style={{ float: "left" }}>
                  <strong> {language === "en" ? "My Bag" : "ব্যাগ"} </strong>({cartList.length})
                </h5>
                <button style={{ float: "right" }} onClick={removeClassName}>
                  <img src={`${process.env.PUBLIC_URL}/assets/images/close-button.png`} className="img-fluid" alt="" />
                  <i className="fa fa-times"></i>
                </button>
              </div>

              {cartList.map((item, index) => (
                <CartPage key={index} item={item} total={total} symbol={symbol} removeFromCart={() => removeFromCart(item)} />
              ))}
              {cartList.length > 0 ? (
                <div>
                  <li>
                    <div className="total">
                      <h5 style={{ color: "black" }}>
                        {language === "en" ? "subtotal :" : "উপসমষ্টি"}{" "}
                        <span>
                          {symbol}
                          {total}
                        </span>
                      </h5>
                    </div>
                  </li>
                  <li>
                    <div className="buttons">
                      <Link
                        to={`${process.env.PUBLIC_URL}/login`}
                        className="btn btn-solid-cart"
                        style={{ marginBottom: "20px", width: "100%", backgroundSize: "620px" }}
                      >
                        <div className="checkout" onClick={removeClassName}>
                          {language === "en" ? "checkout" : "চেকআউট"}
                        </div>
                      </Link>
                      <Link
                        to={`${process.env.PUBLIC_URL}/cart`}
                        className="btn btn-solid-cart"
                        style={{ width: "100%", backgroundSize: "620px" }}
                        onClick={removeClassName}
                      >
                        <div className="view-cart">{language === "en" ? "view cart" : "কার্ট দেখুন"}</div>
                      </Link>
                    </div>
                  </li>
                </div>
              ) : (
                <li>
                  <h5>{language === "en" ? "Your cart is currently empty." : "আপনার কার্ট বর্তমানে খালি।"} </h5>
                </li>
              )}
            </ul>
          </li>
        </ul>
      </div>
    );
  }
}

function addClassName() {
  const temp = document.getElementById("test");
  if (temp.classList.contains("addDiv")) temp.classList.remove("addDiv");
  else {
    temp.classList.add("addDiv");
  }
}
function removeClassName() {
  const temp = document.getElementById("test");
  temp.classList.remove("addDiv");
}

function mapStateToProps(state) {
  return {
    cartList: state.cartList.cart,
    total: getCartTotal(state.cartList.cart),
    symbol: state.data.symbol,
    language: state.Intl.locale
  };
}

export default connect(mapStateToProps, { removeFromCart })(CartContainer);
