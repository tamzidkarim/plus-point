import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ScrollContext } from "react-router-scroll-4";
import { IntlReducer as Intl, IntlProvider } from "react-redux-multilingual";
import "./index.scss";

// Import custom components
import store from "./store";
import translations from "./constants/translations";
import { getAllProducts } from "./actions";

// Layouts
import Vegetables from "./components/layouts/vegetables/main";
import findus from "./components/layouts/vegetables/findusPage";

//Collection Pages
import CollectionLeftSidebar from "./components/collection/collection-left-sidebar";

// Product Pages
import LeftSideBar from "./components/products/left-sidebar";
import LeftImage from "./components/products/left-image";

// Features
import Layout from "./components/app";
import Cart from "./components/cart";
import Compare from "./components/compare/index";
import wishList from "./components/wishlist";
import checkOut from "./components/checkout";
import Paysenz from "./components/paysenz/paysenz";
import orderSuccess from "./components/checkout/success-page";

// Extra Pages
import aboutUs from "./components/pages/about-us";
import CustomerService from "./components/pages/customer-service";
import TermsConditions from "./components/pages/terms-conditions";
import DashboardLeft from "./components/pages/dashboard-left";
import Privacy from "./components/pages/privacy";
import PageNotFound from "./components/pages/404";
import Login from "./components/pages/login";
import Register from "./components/pages/register";
import ForgetPassword from "./components/pages/forget-password";
import Contact from "./components/pages/contact";
import Dashboard from "./components/pages/dashboard";
import ThankYou from "./components/pages/thank-you";
import ChangePass from "./components/pages/change-pass";
import MyOrders from "./components/pages/my-orders";

// Blog Pages
import RightSide from "./components/blogs/right-sidebar";
import Details from "./components/blogs/details";
import CollectionSubDepartments from "./components/collection/collection-subdepartment";

let BASE_URL = "https://pluspointapi.unlockretail.com";
const API_URL = `${BASE_URL}/api/web/v1/product?access-token=9e74904aa7581d08c1465f8fc842630c`;

class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allDataList: []
    };
  }

  componentDidMount() {
    this.getFullProductList();
  }

  getFullProductList = async () => {
    let res = await fetch(API_URL, {
      method: "GET",
      cache: "no-cache"
    }).then(response => {
      return response.json();
    });
    this.setState({ allDataList: res.data });
  };

  render() {
    store.dispatch(getAllProducts(this.state.allDataList));

    return (
      <Provider store={store}>
        <IntlProvider translations={translations} locale="en">
          <BrowserRouter basename={"/"}>
            <ScrollContext>
              <Switch>
                <Route exact path={`${process.env.PUBLIC_URL}/`} component={Vegetables} />
                {/*<Route path={`${process.env.PUBLIC_URL}/vegetables`} component={Vegetables}/>*/}
                <Layout>
                  <Route path={`${process.env.PUBLIC_URL}/products/:category/:subcategory`} component={CollectionLeftSidebar} />
                  <Route path={`${process.env.PUBLIC_URL}/products/:category`} component={CollectionLeftSidebar} />

                  {/*Routes For Features (Product Collection)   Url should Not be changeable last if url change then component will be change*/}
                  <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/top-and-t-shirts`} component={CollectionLeftSidebar} />
                  <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/suits-and-jackets`} component={CollectionLeftSidebar} />
                  <Route
                    path={`${process.env.PUBLIC_URL}/boys-clothing/collection/jeans-tousers-and-shorts`}
                    component={CollectionLeftSidebar}
                  />
                  <Route
                    path={`${process.env.PUBLIC_URL}/boys-clothing/collection/kurtas-and-sherwani`}
                    component={CollectionLeftSidebar}
                  />
                  <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/sets-and-outfits`} component={CollectionLeftSidebar} />
                  <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/shirts`} component={CollectionLeftSidebar} />

                  {/*Routes For Single Product*/}
                  <Route path={`${process.env.PUBLIC_URL}/left-sidebar/product/:id`} component={LeftSideBar} />
                  <Route path={`${process.env.PUBLIC_URL}/product/:id`} component={LeftImage} />

                  {/*Routes For custom Features*/}
                  <Route path={`${process.env.PUBLIC_URL}/cart`} component={Cart} />
                  <Route path={`${process.env.PUBLIC_URL}/wishlist`} component={wishList} />
                  <Route path={`${process.env.PUBLIC_URL}/compare`} component={Compare} />
                  <Route path={`${process.env.PUBLIC_URL}/checkout`} component={checkOut} />
                  <Route path={`${process.env.PUBLIC_URL}/paysenz-order`} component={Paysenz} />
                  <Route path={`${process.env.PUBLIC_URL}/order-success`} component={orderSuccess} />

                  <Route path={`${process.env.PUBLIC_URL}/sales/orders`} component={aboutUs} />

                  {/*Routes For Extra Pages*/}
                  <Route path={`${process.env.PUBLIC_URL}/pages/about-us`} component={aboutUs} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/customer-service`} component={CustomerService} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/terms-conditions`} component={TermsConditions} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/dashboard-left`} component={DashboardLeft} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/privacy`} component={Privacy} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/404`} component={PageNotFound} />
                  <Route path={`${process.env.PUBLIC_URL}/login`} component={Login} />
                  <Route path={`${process.env.PUBLIC_URL}/register`} component={Register} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/forget-password`} component={ForgetPassword} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/contact`} component={Contact} />
                  <Route path={`${process.env.PUBLIC_URL}/dashboard`} component={Dashboard} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/thank-you`} component={ThankYou} />
                  <Route path={`${process.env.PUBLIC_URL}/change-pass`} component={ChangePass} />
                  <Route path={`${process.env.PUBLIC_URL}/my-orders`} component={MyOrders} />
                  <Route path={`${process.env.PUBLIC_URL}/findus`} component={findus} />

                  {/*Blog Pages*/}
                  <Route path={`${process.env.PUBLIC_URL}/blog/right-sidebar`} component={RightSide} />
                  <Route path={`${process.env.PUBLIC_URL}/blog/details`} component={Details} />

                  {/* <Route exact path="*" component={PageNotFound} /> */}
                </Layout>
              </Switch>
            </ScrollContext>
          </BrowserRouter>
        </IntlProvider>
      </Provider>
    );
  }
}

ReactDOM.render(<Root />, document.getElementById("root"));
