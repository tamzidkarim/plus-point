import React, { Component } from "react";
import Breadcrumb from "../common/breadcrumb";
import { Link } from "react-router-dom";
import MyOrders from "./my-orders";
import wishList from "../wishlist/index";
import ChangePass from "./change-pass";
import { connect } from "react-redux";

class DashboardLeft extends Component {
  constructor(props) {
    super(props);
  }

  Logout(event) {
    localStorage.removeItem("token");
    window.location.href = "/login";
  }

  render() {
    const { language } = this.props;
    return (
      <div className="dashboard-left">
        <div className="collection-mobile-back">
          <span className="filter-back">
            <i className="fa fa-angle-left" aria-hidden="true"></i> back
          </span>
        </div>
        <div className="block-content">
          <ul>
            <li className="active">
              <Link to={`${process.env.PUBLIC_URL}/dashboard`}>{language === "en" ? "Account Info" : "অ্যাকাউন্ট তথ্য"}</Link>
            </li>
            <li>
              <Link to={`${process.env.PUBLIC_URL}/my-orders`}>{language === "en" ? "My Orders" : "আমার অর্ডার"}</Link>
            </li>
            <li>
              <Link to={`${process.env.PUBLIC_URL}/wishlist`}>{language === "en" ? "My Wishlist" : "আমার পছন্দ তালিকা"}</Link>
            </li>
            <li>
              <Link to={`${process.env.PUBLIC_URL}/change-pass`}>{language === "en" ? "Change Password" : "পাসওয়ার্ড পরিবর্তন করুন"}</Link>
            </li>
            <li className="last">
              <Link to={"#"} data-lng="en" onClick={this.Logout}>
                {language === "en" ? "Logout" : "লগআউট"}
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(DashboardLeft);
