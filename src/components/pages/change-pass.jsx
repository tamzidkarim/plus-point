import React, { Component } from "react";
import Breadcrumb from "../common/breadcrumb";
import { connect } from "react-redux";
import DashboardLeft from "./dashboard-left";

class ChangePass extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { language } = this.props;
    return (
      <div>
        <Breadcrumb title={"Change Password"} />
        {/*Dashboard section*/}
        <section className="section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-lg-3">
                <div className="account-sidebar">
                  <a className="popup-btn">{language === "en" ? "Change your password" : "আপনার পাসওয়ার্ড পরিবর্তন করুন"}</a>
                </div>
                <DashboardLeft />
              </div>
              <div className="col-lg-9">
                <div className="dashboard-right">
                  {/*Forget Password section*/}
                  <section className="pwd-page" style={{ paddingTop: 0 }}>
                    <div className="container">
                      <div className="row">
                        <div className="col-lg-6 offset-lg-3">
                          <h2>{language === "en" ? "Change your password" : "আপনার পাসওয়ার্ড পরিবর্তন করুন"}</h2>
                          <form className="theme-form">
                            <div className="form-row">
                              <div className="col-md-12">
                                <input type="text" className="form-control" id="old" placeholder="Enter Your Old Password" required="" />
                                <input type="text" className="form-control" id="new" placeholder="Enter Your New Password" required="" />
                                <input type="text" className="form-control" id="confirm" placeholder="Enter Confirm Password" required="" />
                              </div>
                              <a href="#" className="btn btn-solid">
                                {language === "en" ? "Submit" : "জমা দিন"}
                              </a>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps)(ChangePass);
