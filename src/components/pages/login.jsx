/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import Breadcrumb from "../common/breadcrumb";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import FacebookLogin from "react-facebook-login";
import GoogleLogin from "react-google-login";

import { changeUserType } from "../../actions";

class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      Username: "",
      Password: "",
      error_username: "",
      error_password: "",
      register_title: "",
      register_name: "",
      register_username: "",
      register_password: "",
      register_email: "",
      register_gender: "",
      gender: "",
      register_title_error: "",
      register_name_error: "",
      register_username_error: "",
      register_password_error: "",
      register_email_error: "",
      registration_success_message: "",
      msg_token: ""
    };

    this.Password = this.Password.bind(this);
    this.Username = this.Username.bind(this);
    this.login = this.login.bind(this);

    this.register_title = this.register_title.bind(this);
    this.register_name = this.register_name.bind(this);
    this.register_username = this.register_username.bind(this);
    this.register_password = this.register_password.bind(this);
    this.register_email = this.register_email.bind(this);
    this.register_gender = this.register_gender.bind(this);
    this.Registration = this.Registration.bind(this);
  }
  Username(event) {
    this.setState({ Username: event.target.value });
  }
  Password(event) {
    this.setState({ Password: event.target.value });
  }
  register_title(event) {
    this.setState({ register_title: event.target.value });
  }
  register_name(event) {
    this.setState({ register_name: event.target.value });
  }
  register_username(event) {
    this.setState({ register_username: event.target.value });
  }
  register_password(event) {
    this.setState({ register_password: event.target.value });
  }
  register_email(event) {
    this.setState({ register_email: event.target.value });
  }
  // register_gender(event) {
  //     this.setState({ register_gender: event.target.value })
  // }

  register_gender = event => {
    var obj = {};
    obj[event.target.name] = event.target.value;
    this.setState(obj);
  };

  GuestRegistration = () => {
    this.props.changeUserType("Guest");
    localStorage.setItem("token", "guestUser" + new Date().getUTCMilliseconds().toString());
    localStorage.setItem("username", "Guest");
    localStorage.removeItem("custId");
    window.location.href = "/checkout";
  };

  responseFacebook = response => {
    console.log("FACEBOOK", response);
    let fbName = response.name;
    let fbUsername = response.name + "_f";
    let fbEmail;
    if ("email" in response) {
      fbEmail = response.email;
    } else {
      fbEmail = "n/a";
    }
    console.log("email is ", fbEmail);
    let fbPassword = response.userID;
    let userType = this.props.userType;
    if (response.name) {
      fetch("https://pluspointapi.unlockretail.com/api/web/v1/user/signup?access-token=9e74904aa7581d08c1465f8fc842630c", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest"
        },
        body: JSON.stringify({
          userType: "3",
          name: fbName,
          username: fbUsername.replace(/ /g, ""),
          email: fbEmail,
          password: fbPassword,
          title: "n/a",
          gender: "n/a"
        })
      })
        .then(Response => Response.json())
        .then(result => {
          // console.log(result.data);
          if (result.data.status === 422) {
            const user = JSON.parse(result.data.message);
            console.log(result);
            if (user.username[0] == "This username has already been taken.") {
              fetch("https://pluspointapi.unlockretail.com/api/web/v1/user/login?access-token=9e74904aa7581d08c1465f8fc842630c", {
                method: "post",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  "X-Requested-With": "XMLHttpRequest"
                },
                body: JSON.stringify({
                  username: fbUsername.replace(/ /g, ""),
                  password: fbPassword
                })
              })
                .then(Response => Response.json())
                .then(result => {
                  console.log(result.data);
                  console.log(result.data.status);
                  if (result.data.status === 422) {
                    const user = JSON.parse(result.data.message);
                    localStorage.removeItem("token");
                  } else {
                    if (result.data.auth_key) {
                      this.props.changeUserType("Normal");
                      localStorage.setItem("token", result.data.auth_key);
                      localStorage.setItem("username", result.data.username);
                      localStorage.setItem("custId", result.data.custId);
                      localStorage.setItem("author_id", result.data.id);
                      window.location.href = "/checkout";
                    } else {
                      window.location.href = "/login";
                    }
                  }
                });
            }
          } else {
            console.log("RESULTTTTT", result);
            alert("Registering facebook user");
            console.log("Success");
            this.setState({ registration_success_message: "Registration Successfully!!!" });
            localStorage.setItem("custId", result.data.custId);
            localStorage.setItem("username", result.data.username);
            localStorage.setItem("author_id", result.data.id);
            localStorage.setItem("token", result.data.access_token);
            localStorage.setItem("msg_token", this.state.registration_success_message);
            window.location.href = "/checkout";
          }
        });
    }
  };

  responseGoogle = response => {
    console.log("GOOGLE", response);
    console.log(typeof response.profileObj);
    if (typeof response.profileObj !== "undefined") {
      console.log("in");
      let googleName = response.profileObj.name;
      let googleUsername = response.profileObj.name + "_g";
      let googleEmail = response.profileObj.email;
      let googlePassword = response.profileObj.googleId;
      fetch("https://pluspointapi.unlockretail.com/api/web/v1/user/signup?access-token=9e74904aa7581d08c1465f8fc842630c", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest"
        },
        body: JSON.stringify({
          userType: "3",
          name: googleName,
          username: googleUsername.replace(/ /g, ""),
          email: googleEmail,
          password: googlePassword
        })
      })
        .then(Response => Response.json())
        .then(result => {
          console.log(result.data);
          if (result.data.status === 422) {
            const user = JSON.parse(result.data.message);
            console.log(result);
            if (user.username[0] == "This username has already been taken.") {
              fetch("https://pluspointapi.unlockretail.com/api/web/v1/user/login?access-token=9e74904aa7581d08c1465f8fc842630c", {
                method: "post",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  "X-Requested-With": "XMLHttpRequest"
                },
                body: JSON.stringify({
                  username: googleUsername.replace(/ /g, ""),
                  password: googlePassword
                })
              })
                .then(Response => Response.json())
                .then(result => {
                  console.log(result.data);
                  console.log(result.data.status);
                  if (result.data.status === 422) {
                    const user = JSON.parse(result.data.message);
                    localStorage.removeItem("token");
                  } else {
                    if (result.data.auth_key) {
                      this.props.changeUserType("Normal");
                      localStorage.setItem("token", result.data.auth_key);
                      localStorage.setItem("username", result.data.username);
                      localStorage.setItem("custId", result.data.custId);
                      localStorage.setItem("author_id", result.data.id);
                      window.location.href = "/checkout";
                    } else {
                      window.location.href = "/login";
                    }
                  }
                });
            }
          } else {
            console.log(result.data);
            console.log("Success");
            this.setState({ registration_success_message: "Registration Successfully!!!" });
            localStorage.setItem("custId", result.data.custId);
            localStorage.setItem("username", result.data.username);
            localStorage.setItem("author_id", result.data.id);
            localStorage.setItem("token", result.data.access_token);
            localStorage.setItem("msg_token", this.state.registration_success_message);
            window.location.href = "/checkout";
          }
        });
    }
  };

  login(event) {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/user/login?access-token=9e74904aa7581d08c1465f8fc842630c", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: JSON.stringify({
        username: this.state.Username,
        password: this.state.Password
      })
    })
      .then(Response => Response.json())
      .then(result => {
        console.log(result.data);
        console.log(result.data.status);
        if (result.data.status === 422) {
          const user = JSON.parse(result.data.message);
          localStorage.removeItem("token");
          this.setState({ error_username: user.username ? user.username : "" });
          this.setState({ error_password: user.password ? user.password : "" });
        } else {
          if (result.data.auth_key) {
            this.props.changeUserType("Normal");
            localStorage.setItem("token", result.data.auth_key);
            localStorage.setItem("username", result.data.username);
            localStorage.setItem("custId", result.data.custId);
            localStorage.setItem("author_id", result.data.id);
            window.location.href = "/checkout";
          } else {
            window.location.href = "/login";
          }
        }
      });
  }

  //Registrations
  Registration(event) {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/user/signup?access-token=9e74904aa7581d08c1465f8fc842630c", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: JSON.stringify({
        userType: "3",
        title: this.state.register_title,
        name: this.state.register_name,
        username: this.state.register_username,
        password: this.state.register_password,
        email: this.state.register_email,
        gender: this.state.register_gender
      })
    })
      .then(Response => Response.json())
      .then(result => {
        console.log("grgrgrgrgr", result);
        if (result.data.status === 422) {
          const user = JSON.parse(result.data.message);
          this.setState({ register_name_error: user.username ? user.username : "" });
          this.setState({ register_password_error: user.password ? user.password : "" });
        } else {
          console.log("hththththt", result);
          console.log("Success");
          this.setState({ registration_success_message: "Registration Successfully!!!" });
          this.props.changeUserType("Normal");
          localStorage.setItem("username", result.data._user.username);
          localStorage.setItem("custId", result.data._user.custId);
          localStorage.setItem("author_id", result.data._user.id);
          localStorage.setItem("token", result.data._user.auth_key);
          alert("registered!!");
          // localStorage.setItem("msg_token", this.state.registration_success_message);
          window.location.href = "/checkout";
        }
      });
  }

  render() {
    const { language } = this.props;
    /*  localStorage.removeItem('token');*/
    var tokenCheck = localStorage.getItem("token");
    if (tokenCheck) {
      this.props.history.push("/checkout");
    }
    return (
      <div>
        <Breadcrumb title={"Login"} />
        {/*Login section*/}
        <section className="login-page section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-lg-4">
                <h3>{language === "en" ? "Sign in" : "সাইন ইন করুন"}</h3>
                <div className="theme-card">
                  <form className="theme-form">
                    <div className="form-group">
                      <label htmlFor="email">
                        {language === "en" ? "Username" : "ব্যবহারকারীর নাম"}
                        <span className="error">*</span>
                      </label>
                      <input type="text" onChange={this.Username} className="form-control" id="email" placeholder="Username" required="" />
                      <span className="error">{this.state.error_username}</span>
                    </div>
                    <div className="form-group">
                      <label htmlFor="review">
                        {language === "en" ? "Password" : "পাসওয়ার্ড"}
                        <span className="error">*</span>
                      </label>
                      <input
                        type="password"
                        onChange={this.Password}
                        className="form-control"
                        id="password"
                        placeholder="Enter your password"
                        required=""
                      />
                      <span className="error">{this.state.error_password}</span>
                    </div>
                    <a style={{color:"#fff"}} className="btn btn-solid" onClick={this.login}>
                      {language === "en" ? "Login" : "লগইন"}
                    </a>
                    <a style={{ marginLeft: "20px" }} href="!#">
                      {language === "en" ? "Forgot password?" : "পাসওয়ার্ড ভুলে গেছেন?"}
                    </a>
                    <div>
                      <h5 style={{ margin: "30px 0 15px 0" }}>
                        <strong> {language === "en" ? "OR Sign in With" : "বা সাইন ইন করুন"}</strong>
                      </h5>
                    </div>
                    <div>
                      {/* <a class="button button--social-login button--facebook" href="#">
                                            <i class="icon fa fa-facebook"></i>Login With Facebook
                                          </a> */}
                      <FacebookLogin
                        appId="1050866991926555"
                        autoLoad={false}
                        fields="name,email,picture"
                        callback={this.responseFacebook}
                        cssClass="button button--social-login button--facebook"
                        icon="fa fa-facebook"
                      ></FacebookLogin>
                    </div>
                    <div>
                      {/* <a className="button button--social-login button--google" href="!#">
                        <i className="icon fa fa-google"></i>Login With Google{language === "en" ? "" : ""}
                      </a> */}
                      <GoogleLogin
                        clientId="232870750903-5ikvr5i3eo5o27u86q3ffgj92jj89mav.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={this.responseGoogle}
                        onFailure={this.responseGoogle}
                      />
                    </div>
                  </form>
                </div>
              </div>
              <div className="col-lg-8 right-login">
                <h3>{language === "en" ? "No Account? Register" : "কোন অ্যাকাউন্ট নেই? নিবন্ধন"}</h3>
                <div className="theme-card">
                  <form className="theme-form">
                    <div className="form-row">
                      <div className="col-md-4" style={{marginBottom:"30px"}}>
                        <label htmlFor="name">
                          {language === "en" ? "Name" : "নাম"}
                          <span className="error">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="name"
                          placeholder="Name"
                          required=""
                          name="register_name"
                          onChange={this.register_name}
                        />
                        <span className="error">{this.state.register_name_error}</span>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="username">
                          {language === "en" ? "Username" : "ব্যবহারকারীর নাম"}
                          <span className="error">*</span>
                        </label>

                        <input
                          type="text"
                          className="form-control"
                          id="username"
                          placeholder="User Name"
                          required=""
                          name="register_username"
                          onChange={this.register_username}
                        />
                        <span className="error">{this.state.register_username_error}</span>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="title">
                          {language === "en" ? "Mobile Number" : "মোবাইল নম্বর"}
                          <span className="error">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="title"
                          placeholder="Mobile Number"
                          required=""
                          name="register_title"
                          onChange={this.register_title}
                        />
                      </div>
                    </div>

                    <div className="form-row margin-10">
                      <div className="col-md-4" style={{marginBottom:"30px"}}>
                        <label htmlFor="email">
                          {language === "en" ? "email" : "ই-মেইল"}
                          <span className="error">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="register_email"
                          placeholder="sometings@gmail.com"
                          name="register_email"
                          onChange={this.register_email}
                        />
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="password">
                          {language === "en" ? "Password" : "পাসওয়ার্ড"}
                          <span className="error">*</span>
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          id="register_password"
                          placeholder="Enter you password"
                          required=""
                          name="register_password"
                          onChange={this.register_password}
                        />
                        <span className="error">{this.state.register_password_error}</span>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="password">
                          {language === "en" ? "Confirm Password" : "পাসওয়ার্ড নিশ্চিত করুন"}
                          <span className="error">*</span>
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          id="register_password"
                          placeholder="Enter you password"
                          required=""
                          name="register_password"
                          onChange={this.register_password}
                        />
                        <span className="error">{this.state.register_password_error}</span>
                      </div>
                    </div>
                    <a style={{color:"#FFF"}} className="btn btn-solid" onClick={this.Registration}>
                      {language === "en" ? "create Account" : "অ্যাকাউন্ট তৈরি"}
                    </a>
                    <span style={{ margin: "0 20px" }}>OR</span>
                    <a style={{ background: "#666666",color:"#fff" }} className="btn btn-solid" onClick={this.GuestRegistration}>
                      {language === "en" ? "Guest checkout" : "অতিথি চেকআউট"}
                    </a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps, {
  changeUserType
})(Login);
