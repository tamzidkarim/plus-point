import React, { Component } from "react";
import { connect } from "react-redux";
import Breadcrumb from "../common/breadcrumb";

class Contact extends Component {
  constructor(props) {
    super(props);
  }

  render() {
      const {language}= this.props;
    return (
      <div>
        <Breadcrumb title={language === "en" ? "Contact Us" : "যোগাযোগ করুন"} />

        {/*Forget Password section*/}
        <section className=" contact-page section-b-space">
          <div className="container">
            <div className="row section-b-space">
              <div className="col-lg-7 map">
                <div className="mapouter">
                  <div className="gmap_canvas">
                    <iframe
                      style={{ height: "500px" }}
                      width="600"
                      height="500"
                      id="gmap_canvas"
                      src="https://maps.google.com/maps?q=plus%20point&t=&z=11&ie=UTF8&iwloc=&output=embed"
                      frameborder="0"
                      scrolling="no"
                      marginheight="0"
                      marginwidth="0"
                    ></iframe>
                  </div>
                </div>
              </div>
              <div className="col-lg-5">
                <div className="contact-right">
                  <ul>
                    <li>
                      <div className="contact-icon">
                        <img src={`${process.env.PUBLIC_URL}/assets/images/icon/phone.png`} alt="Generic placeholder image" />
                        <h6>{language === "en" ? "Contact Us" : "যোগাযোগ করুন"} </h6>
                      </div>
                      <div className="media-body">
                        <p>{language === "en" ? "+8801711111111" : "8801711111111"} </p>
                        <p>{language === "en" ? "+8801711111111" : "8801711111111"} </p>
                      </div>
                    </li>
                    <li>
                      <div className="contact-icon">
                        <i className="fa fa-map-marker" aria-hidden="true"></i>
                        <h6>{language === "en" ? "Address" : "ঠিকানা"} </h6>
                      </div>
                      <div className="media-body">
                        <p>{language === "en" ? "House# 654, Road#9" : "বাড়ি # 654, রোড # 9"} </p>
                        <p>{language === "en" ? "DOHS Mirpur" : "ডিওএইচএস মিরপুর"} </p>
                      </div>
                    </li>
                    <li>
                      <div className="contact-icon">
                        <img src={`${process.env.PUBLIC_URL}/assets/images/icon/email.png`} alt="Generic placeholder image" />
                        <h6>{language === "en" ? "Address" : "ঠিকানা"} </h6>
                      </div>
                      <div className="media-body">
                        <p>{language === "en" ? "info@pluspointbd.com" : "info@pluspointbd.com"} </p>
                        <p>{language === "en" ? "support@pluspointbd.com" : "support@pluspointbd.com"} </p>
                      </div>
                    </li>
                    <li>
                      <div className="contact-icon">
                        <i className="fa fa-fax" aria-hidden="true"></i>
                        <h6>{language === "en" ? "Fax" : "ফ্যাক্স"} </h6>
                      </div>
                      <div className="media-body">
                        <p>{language === "en" ? "+8801711111111" : "+8801711111111"} </p>
                        <p>{language === "en" ? "support@pluspointbd.com" : "support@pluspointbd.com"} </p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <form className="theme-form">
                  <div className="form-row">
                    <div className="col-md-6">
                      <label htmlFor="name">{language === "en" ? "First Name" : "প্রথম নাম"} </label>
                      <input type="text" className="form-control" id="name" placeholder={language === "en" ? "First Name" : "প্রথম নাম"} required />
                    </div>
                    <div className="col-md-6">
                      <label htmlFor="email">{language === "en" ? "Last Name" : "নামের শেষাংশ"} </label>
                      <input type="text" className="form-control" id="last-name" placeholder={language === "en" ? "Last Name" : "নামের শেষাংশ"} required/>
                    </div>
                    <div className="col-md-6">
                      <label htmlFor="review">{language === "en" ? "Phone number" : "ফোন নম্বর"} </label>
                      <input type="text" className="form-control" id="review" placeholder={language === "en" ? "Phone number" : "ফোন নম্বর"} required />
                    </div>
                    <div className="col-md-6">
                      <label htmlFor="email">{language === "en" ? "Email" : "ইমেইল"} </label>
                      <input type="text" className="form-control" id="email" placeholder={language === "en" ? "Email" : "ইমেইল"} required />
                    </div>
                    <div className="col-md-12">
                      <label htmlFor="review">{language === "en" ? "Write Your Message" : "আপনার বার্তা লিখুন"} </label>
                      <textarea
                        className="form-control"
                        placeholder={language === "en" ? "Write Your Message" : "আপনার বার্তা লিখুন"}
                        id="exampleFormControlTextarea1"
                        rows="6"
                      ></textarea>
                    </div>
                    <div className="col-md-12">
                      <button className="btn btn-solid" type="submit">
                        {language === "en" ? "Send Your Message" : "আপনার বার্তা প্রেরণ"} 
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps) (Contact);
