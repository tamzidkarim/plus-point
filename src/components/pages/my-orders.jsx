import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Breadcrumb from "../common/breadcrumb";
import { addToCartAndRemoveWishlist, removeFromWishlist } from "../../actions";
import DashboardLeft from "./dashboard-left";
import Axios from "axios";

class MyOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderitems: [],
      token: "",
      custId: ""
    };
  }

  componentDidMount() {
    const tokenVar = localStorage.getItem("token");
    const custId = localStorage.getItem("custId");
    if (tokenVar) {
      this.setState({ custId });
      this.setState({ token: tokenVar });
      this.getOrderList(custId);
    }
  }

  getOrderList(custId) {
    fetch(
      `https://pluspointapi.unlockretail.com/api/web/v1/orders?access-token=9e74904aa7581d08c1465f8fc842630c&filter={"custId":${custId}}&sort=-id`,
      {
        method: "GET"
      }
    )
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        // console.log("Orders fetched for user", myJson.data);
        this.setState({ orderitems: myJson.data });
      });
    // Axios.get(
    //   `https://pluspointapi.unlockretail.com/api/web/v1/orders?access-token=9e74904aa7581d08c1465f8fc842630c&filter={"custId":${custId}}&sort=-id`
    // ).then(response => {
    //   // console.log("Orders fetched for user", response.data.data);
    //   this.setState({ orderitems: response.data.data });
    // });
  }

  checkPaymentType(item) {
    if (item.cashPaid) return "Cash";
    else if (item.cashPaid) return "Cash";
    else return "Submitted";
  }

  changeQty = e => {
    this.setState({ quantity: parseInt(e.target.value) });
  };

  render() {
    const { orderitems, token } = this.state;
    const { language } = this.props;
    //Session Destroy
    if (token == null || typeof token == "undefined") {
      this.props.history.push("/login");
    }
    return (
      <div>
        <Breadcrumb title={"My Orders"} />
        {/*Dashboard section*/}
        <section className="section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-lg-3">
                <div className="account-sidebar">
                  <a className="popup-btn">my account</a>
                </div>
                <DashboardLeft />
              </div>
              <div className="col-lg-9">
                <div className="dashboard-right">
                  <div>
                    {orderitems.length > 0 ? (
                      <table className="table cart-table order-table-responsive">
                        <thead>
                          <tr className="table-head">
                            <th scope="col">{language === "en" ? "INDEX" : "ইনডেক্স"}</th>
                            <th scope="col">{language === "en" ? "Total Price" : "মোট দাম"}</th>
                            <th scope="col">{language === "en" ? "Payment Status" : "লেনদেনের অবস্থা"}</th>
                            <th scope="col">{language === "en" ? "Payment Type" : "অর্থপ্রদানের ধরণ"}</th>
                            <th scope="col">{language === "en" ? "Created At" : "তৈরি করা হয়েছে"}</th>
                          </tr>
                        </thead>
                        {orderitems.slice(0, 15).map((item, index) => {
                          return (
                            <tbody key={index++}>
                              <tr>
                                <td>
                                  <p style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>{index}</p>
                                </td>
                                {/* <td>
                                  {" "}
                                  <img
                                    height="50"
                                    width="50"
                                    src={item.billing.billing_address.image ? item.billing.billing_address.image : ""}
                                    alt=""
                                  />
                                </td> */}
                                {/* <td>
                                  <p style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>{item.id}</p>
                                </td> */}
                                <td>
                                  <p style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>
                                    {item.totalPrice}
                                  </p>
                                </td>
                                <td>
                                  <p style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>
                                    {item.status == 3 ? "Processing" : "Paid"}
                                  </p>
                                </td>
                                <td>
                                  <p style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>
                                    {this.checkPaymentType(item)}
                                  </p>
                                </td>
                                <td>
                                  <p style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>
                                    {item.crAt}
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          );
                        })}
                      </table>
                    ) : (
                      <section className="cart-section section-b-space">
                        <div className="container">
                          <div className="row">
                            <div className="col-sm-12">
                              <div>
                                <div className="col-sm-12 empty-cart-cls text-center">
                                  <img
                                    src={`${process.env.PUBLIC_URL}/assets/images/empty-wishlist.png`}
                                    className="img-fluid mb-4"
                                    alt=""
                                  />
                                  <h3>
                                    <strong>{language === "en" ? "Order List is Empty" : "অর্ডার তালিকা খালি"}</strong>
                                  </h3>
                                  <h4>
                                    {language === "en" ? "Explore more shortlist some items." : "কিছু শর্টলিস্ট কিছু আইটেম এক্সপ্লোর করুন।"}
                                  </h4>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  Items: state.wishlist.list,
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, { addToCartAndRemoveWishlist, removeFromWishlist })(MyOrders);
