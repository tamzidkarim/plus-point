import React, { Component } from "react";
import Breadcrumb from "../common/breadcrumb";
import { connect } from "react-redux";
import DashboardLeft from "./dashboard-left";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      profileItems: ""
    };
  }
  componentWillMount() {
    const tokenVar = localStorage.getItem("token");
    const custId = localStorage.getItem("custId");
    if (tokenVar) {
      this.getProfile(custId);
    }
  }

  getProfile(custId) {
    fetch(
      `https://pluspointapi.unlockretail.com/api/web/v1/customers/${custId}?access-token=9e74904aa7581d08c1465f8fc842630c&expand=customer`,
      {
        method: "GET"
      }
    )
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        // console.log("USER", myJson.data);
        this.setState({ profileItems: myJson.data });
      });
  }

  render() {
    var tokenCheck = localStorage.getItem("token");
    if (tokenCheck == null || typeof tokenCheck == "undefined") {
      this.props.history.push("/login");
    }
    const { profileItems } = this.state;
    const { language } = this.props;

    // console.log(profileItems);
    return (
      <div>
        <Breadcrumb title={"Dashboard"} />
        {/*Dashboard section*/}
        <section className="section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-lg-3">
                <div className="account-sidebar">
                  <a className="popup-btn">my account</a>
                </div>
                <DashboardLeft />
              </div>
              <div className="col-lg-9">
                <div className="dashboard-right">
                  <div className="dashboard">
                    <div className="page-title">
                      <h2>{language === "en" ? "Dashboard" : "ড্যাশবোর্ড"}</h2>
                    </div>
                    <div className="welcome-msg">
                      <p>
                        {language === "en" ? " Hello, " : "হ্যালো, "} {profileItems.name} !
                      </p>
                    </div>
                    <div className="box-account box-info">
                      <div className="box-head">
                        <h2>{language === "en" ? "Account Information" : "অ্যাকাউন্ট তথ্য"}</h2>
                      </div>
                      <div>
                        <div className="box">
                          <div className="box-title">
                            <h3>{language === "en" ? "Contact Information" : "যোগাযোগের তথ্য"}</h3>
                            <a href="#">{language === "en" ? "Manage Contact" : "যোগাযোগ পরিচালনা"}</a>
                          </div>
                          <div className="row">
                            <div className="col-sm-6">
                              <h6>{profileItems.name}</h6>
                              <h6>{profileItems.email}</h6>
                              <h6>{profileItems.phone}</h6>
                              <h6>
                                <a href={`${process.env.PUBLIC_URL}/change-pass`}>
                                  {language === "en" ? "Change Password" : "পাসওয়ার্ড পরিবর্তন করুন"}
                                </a>
                              </h6>
                            </div>
                            <div className="col-sm-6">
                              <div className="col-md-3">
                                <img src={profileItems.picture_thumb} className="img img-rounded img-fluid" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div>
                        <div className="box">
                          <div className="box-title">
                            <h3>{language === "en" ? "Address Book" : "ঠিকানা"}</h3>
                            {/*<a href="#">Manage Addresses</a>*/}
                          </div>
                          <div className="row">
                            <div className="col-sm-6">
                              <h6> {language === "en" ? "Billing Address" : "বিলিং ঠিকানা"} </h6>
                              <address>
                                {language === "en"
                                  ? "You have not set a default billing address."
                                  : "আপনি একটি ডিফল্ট বিলিং ঠিকানা সেট করেন নি।"}
                                <br />
                              </address>
                            </div>
                            <div className="col-sm-6">
                              <h6>{language === "en" ? "Default Shipping Address" : "ডিফল্ট শিপিং ঠিকানা"}</h6>
                              <address>
                                {language === "en"
                                  ? "You have not set a default shipping address."
                                  : "আপনি কোনও ডিফল্ট শিপিং ঠিকানা সেট করেন নি"}
                                <br />
                              </address>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(Dashboard);
