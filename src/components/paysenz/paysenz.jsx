import React, { Component } from "react";

const requestData = {
  grant_type: "password",
  client_id: 16,
  client_secret: "2XeYHZSAorBmqowLmCOUXFWLTb21kGRlekEfCe46",
  username: "info@unlocklive.com",
  password: "123456",
  scope: "*"
};
class Paysenz extends Component {
  constructor() {
    super();
    this.state = {
      paysenztoken: "",
      orderId: "",
      successUrl: "https://pluspointweb.unlockretail.com/order-success", //`${process.env.PUBLIC_URL}/order-success`
      failUrl: "https://pluspointweb.unlockretail.com/order-success", //`${process.env.PUBLIC_URL}/order-success`,
      cancelUrl: "https://pluspointweb.unlockretail.com/order-success", //`${process.env.PUBLIC_URL}/order-success`,
      ipnUrl: "https://pluspointapi.unlockretail.com/api/web/v1/order/success-order?access-token=9e74904aa7581d08c1465f8fc842630c" //`${process.env.PUBLIC_URL}/order-success`,
    };
  }
  componentWillMount() {
    this.setState({ orderId: 1 + Math.random() * (100 - 1) });
    this.getRetriveToken(requestData);
  }
  //Retrive Access Token from paysenz
  getRetriveToken = requestData => {
    fetch("https://gopaysenz.com/oauth/token", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: JSON.stringify({
        grant_type: "password",
        client_id: "50",
        client_secret: "cOu4BDdisxAoA1XudnJDhucprf2WfGsTdC8DVc6H",
        username: "pluspoint03@gmail.com",
        password: "01521331789",
        scope: "*"
      })
    })
      .then(resp => resp.json())
      .then(data => {
        this.setState({ paysenztoken: data });
        console.log("FUCJ", this.props.billing);
        this.sendInformationToPaysenz(data.access_token, this.props.billing);
      });
  };

  sendInformationToPaysenz(token, data) {
    console.log("Billing Information for paysenz");
    console.log(data);
    if (data) {
      let Json = {
        buyer_name: data.first_name ? data.first_name : "",
        buyer_email: data.email ? data.email : "",
        buyer_address: data.address ? data.address : "",
        cus_city: "City",
        cus_state: "State",
        cus_postcode: data.pincode ? data.pincode : "",
        cus_country: "Bangladesh",
        buyer_contact_number: data.phone ? data.phone : "",
        client_id: requestData.client_id ? requestData.client_id : "",
        order_id_of_merchant: this.state.orderId ? "Paysez" + this.state.orderId : "",
        amount: "1", //this.props.orderTotal ? String(this.props.orderTotal ): '',
        currency_of_transaction: "BDT",
        callback_success_url: this.state.successUrl ? this.state.successUrl : "",
        callback_fail_url: this.state.failUrl ? this.state.failUrl : "",
        callback_cancel_url: this.state.cancelUrl ? this.state.cancelUrl : "",
        callback_ipn_url: this.state.ipnUrl ? this.state.ipnUrl : "",
        order_details: this.state.orderId ? "Payment for ApplicationID:" + this.state.orderId : "",
        expected_response_type: "JSON",
        custom_1: data.order_id ? String(data.order_id) : "",
        custom_2: data.order_number ? String(data.order_number) : "",
        custom_3: data.user_id ? String(data.user_id) : "",
        custom_4: ""
      };
      fetch("https://gopaysenz.com/api/v1.0/pay", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest",
          Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(Json)
      })
        .then(resp => resp.json())
        .then(paysenz_response => {
          if (paysenz_response.expected_response) {
            const url = paysenz_response.expected_response;
            window.location = url;
            console.log(Json);
          }
        });
    }
  }

  render() {
    return (
      <div className="col-lg-12">
        <h3 className="loder_style" style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>
          Your Order Is Processing
          <img
            style={{ width: "10%", display: "inline-block", marginLeft: "20px" }}
            src={`${process.env.PUBLIC_URL}/assets/images/icon/loding.gif`}
          />
        </h3>
      </div>
    );
  }
}

export default Paysenz;
