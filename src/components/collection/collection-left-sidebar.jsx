import React, { Component } from "react";
import { Helmet } from "react-helmet";
import Breadcrumb from "../common/breadcrumb";
import NewProduct from "../common/new-product";
import Filter from "./common/filter";
import FilterBar from "./common/filter-bar";
import ProductListing from "./common/product-listing";
import ProductListingTrading from "./common/product-listing-trading";
import StickyBox from "react-sticky-box";

import { filterDepartment } from "../../actions";
import { connect } from "react-redux";

class CollectionLeftSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      layoutColumns: 3,
      category: ''

    };
    let temp = [];
    try {
      const subDepartment = this.props.location.pathname.split('/')[3].toString();
      this.props.filterDepartment(subDepartment);
    }
    catch {
      const department = this.props.location.pathname.split('/')[2].toString();
      this.props.filterDepartment(department);
    }
  }

  LayoutViewClicked(colums) {
    this.setState({
      layoutColumns: colums
    });
  }

  openFilter = () => {
    document.querySelector(".collection-filter").style = "left: -15px";
  };

  render() {
    const { department } = this.props;

    return (
      <div>
        {/*SEO Support*/}
        <Helmet>
          <title>Pluspoint | Collection of Products</title>
          <meta
            name="description"
            content="Multikart – Multipurpose eCommerce React Template is a multi-use React template. It is designed to go well with multi-purpose websites. Multikart Bootstrap 4 Template will help you run multiple businesses."
          />
        </Helmet>
        {/*SEO Support End */}

        <Breadcrumb title={"Collection"} />

        <section className="section-b-space bottom0">
          <div className="collection-wrapper">
            <div className="container">
              <div className="row">
                <div className="col-sm-3 collection-filter">
                  <StickyBox offsetTop={20} offsetBottom={20}>
                    <div>
                      <Filter category={department} />
                      <NewProduct category={department} />
                      <div className="collection-sidebar-banner"></div>
                    </div>
                  </StickyBox>
                  {/*side-bar banner end here*/}
                </div>
                <div className="collection-content col-md-9">
                  <div className="page-main-content ">
                    <div className="">
                      <div className="row">
                        <div className="col-sm-12">
                          <div className="collection-product-wrapper">
                            <div className="product-top-filter">
                              <div className="container-fluid p-0">
                                <div className="row">
                                  <div className="col-12">
                                    <FilterBar onLayoutViewClicked={colmuns => this.LayoutViewClicked(colmuns)} category={department} colSize={this.state.layoutColumns} />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/*Products Listing Component*/}
                            <ProductListing category={department} colSize={this.state.layoutColumns} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <ProductListingTrading category={department} colSize={this.state.layoutColumns} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  department: state.filters.department
});


export default connect(mapStateToProps,{filterDepartment}) (CollectionLeftSidebar);
