import React, { Component } from "react";
import { connect } from "react-redux";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { SlideToggle } from "react-slide-toggle";

import {
  getBrands,
  getBrandsSubdepartment,
  getColors,
  getColorsSubdepartments,
  getMatchProductSubdepartments,
  getMinMaxPrice
} from "../../../services";
import { filterBrand, filterColor, filterPrice } from "../../../actions";

class FilterSubdepartment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openFilter: false,
      departmentList: []
    };
  }
  componentDidMount() {
    this.fetchDepartment();
  }

  closeFilter = () => {
    document.querySelector(".collection-filter").style = "left: -365px";
  };

  clickBrandHendle(event, brands) {
    var index = brands.indexOf(event.target.value);
    if (event.target.checked) brands.push(event.target.value);
    // push in array checked value
    /* alert("Unchecked");*/ else brands.splice(index, 1); // removed in array unchecked value

    this.props.filterBrand(brands);
  }

  colorHandle(event, color) {
    var elems = document.querySelectorAll(".color-selector ul li");
    [].forEach.call(elems, function(el) {
      el.classList.remove("active");
    });
    event.target.classList.add("active");
    this.props.filterColor(color);
  }

  fetchDepartment() {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/department?access-token=9e74904aa7581d08c1465f8fc842630c", {
      method: "GET"
    })
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        var departmentList = myJson.data.slice(0, 3);
        this.setState({
            departmentList: departmentList
        });
      });
    // Axios.get("https://pluspointapi.unlockretail.com/api/web/v1/department?access-token=9e74904aa7581d08c1465f8fc842630c").then(data => {
    //     var departmentList = data.data.data.slice(0, 3);
    //     this.setState({
    //         departmentList: departmentList
    //     });
    // });
  }

  render() {
    const filteredBrands = this.props.filters.brand;
    var { departmentList } = this.state;
    var { category } = this.props;

    return (
      <div className="collection-filter-block">
        {/*brand filter start*/}
        <div className="collection-mobile-back">
          <span className="filter-back" onClick={e => this.closeFilter(e)}>
            <i className="fa fa-angle-left" aria-hidden="true"></i> back
          </span>
        </div>
        <div>
          <h4 className="filter-title">Collection</h4>
        </div>
        {departmentList.map((department, index) => (
          <SlideToggle>
            {({ onToggle, setCollapsibleElement }) => (
              <div className="collection-collapse-block">
                <h3 className="collapse-block-title" onClick={onToggle}>
                  {department.department_name}
                </h3>
                <div className="collection-collapse-block-content" ref={setCollapsibleElement}>
                  <div className="collection-brand-filter">
                    {department.subdepartment.slice(0, 3).map((subdepartment, index) => {
                      return (
                        <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                          <input
                            type="checkbox"
                            onClick={e => this.clickBrandHendle(e, filteredBrands)}
                            value={subdepartment.subdepartment_name}
                            defaultChecked={filteredBrands.includes("") ? true : false}
                            className="custom-control-input"
                            id={subdepartment.subdepartment_name}
                          />
                          <label className="custom-control-label" htmlFor={subdepartment.subdepartment_name}>
                            {" "}
                            {subdepartment.subdepartment_name}
                          </label>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            )}
          </SlideToggle>
        ))}

        {/*price filter start here */}
        <SlideToggle>
          {({ onToggle, setCollapsibleElement }) => (
            <div className="collection-collapse-block open">
              <h3 className="collapse-block-title" onClick={onToggle}>
                price
              </h3>
              <div className="collection-collapse-block-content block-price-content" ref={setCollapsibleElement}>
                <div className="collection-brand-filter">
                  <div className="custom-control custom-checkbox collection-filter-checkbox">
                    <InputRange
                      maxValue={this.props.prices.max}
                      minValue={this.props.prices.min}
                      value={this.props.filters.value}
                      onChange={value => this.props.filterPrice({ value })}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}
        </SlideToggle>
        <SlideToggle>
          {({ onToggle, setCollapsibleElement }) => (
            <div className="collection-collapse-block">
              <h3 className="collapse-block-title" onClick={onToggle}>
                brand
              </h3>
              <div className="collection-collapse-block-content" ref={setCollapsibleElement}>
                <div className="collection-brand-filter">
                  {this.props.brands.map((brand, index) => {
                    return (
                      <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                        <input
                          type="checkbox"
                          onClick={e => this.clickBrandHendle(e, filteredBrands)}
                          value={brand}
                          defaultChecked={filteredBrands.includes(brand) ? true : false}
                          className="custom-control-input"
                          id={brand}
                        />
                        <label className="custom-control-label" htmlFor={brand}>
                          {brand}
                        </label>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          )}
        </SlideToggle>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  brands: getBrandsSubdepartment(state.data.products, ownProps.category),
  colors: getColorsSubdepartments(state.data.products, ownProps.category),
  prices: getMatchProductSubdepartments(state.data.products, ownProps.category),
  filters: state.filters
});

export default connect(mapStateToProps, { filterBrand, filterColor, filterPrice })(FilterSubdepartment);
