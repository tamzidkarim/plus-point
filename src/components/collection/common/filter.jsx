import React, { Component } from "react";
import { connect } from "react-redux";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { SlideToggle } from "react-slide-toggle";

import { getBrands, getColors, getMinMaxPrice } from "../../../services";
import { filterBrand, filterColor, filterPrice, filterDepartment } from "../../../actions";

class Filter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openFilter: false,
      departmentList: [],
      category: null,
      value: -1
    };
    this.props.filterPrice("none");
  }
  componentDidMount() {
    this.fetchDepartment();
  }

  onChangePriceFilter = value => {
    console.log(value);
    this.setState({ value: value });
    this.props.filterPrice(value);
  };

  closeFilter = () => {
    document.querySelector(".collection-filter").style = "left: -365px";
  };

  clickBrandHendle(event, brands) {
    var index = brands.indexOf(event.target.value);
    if (event.target.checked) brands.push(event.target.value);
    // push in array checked value
    /* alert("Unchecked");*/ else brands.splice(index, 1); // removed in array unchecked value

    this.props.filterBrand(brands);
  }

  clickDepartmentHandle(event, dep) {
    let department;
    if (!Array.isArray(dep)) {
      department = [dep];
    } else {
      department = dep;
    }
    var index = department.indexOf(event.target.value);
    if (event.target.checked) department.push(event.target.value);
    else department.splice(index, 1);
    this.props.filterDepartment(department);
  }

  colorHandle(event, color) {
    var elems = document.querySelectorAll(".color-selector ul li");
    [].forEach.call(elems, function(el) {
      el.classList.remove("active");
    });
    event.target.classList.add("active");
    this.props.filterColor(color);
  }

  fetchDepartment() {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/department?access-token=9e74904aa7581d08c1465f8fc842630c", {
      method: "GET"
    })
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        var departmentList = myJson.data.slice(0, 3);
        this.setState({
          departmentList: departmentList
        });
      });
  }

  render() {
    const filteredBrands = this.props.filters.brand;
    var { departmentList } = this.state;
    var selectedDepartment = this.props.filters.department;
    var { language, symbol } = this.props;

    return (
      <div className="collection-filter-block">
        {/*brand filter start*/}
        <div className="collection-mobile-back">
          <span className="filter-back" onClick={e => this.closeFilter(e)}>
            back
          </span>
        </div>
        <div className="">
          <SlideToggle duration={100}>
            {({ onToggle, setCollapsibleElement }) => (
              <div className="collection-collapse-block">
                <h4 className="collapse-block-title" onClick={onToggle}>
                  {language === "en" ? "Collection" : "সংগ্রহ"}
                </h4>

                <div className="collection-filter-toggle" ref={setCollapsibleElement}>
                  {departmentList.map((department, index) => (
                    <div>
                      <div style={{ marginTop: "10px" }} className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                        <input
                          type="checkbox"
                          onClick={e => this.clickDepartmentHandle(e, selectedDepartment)}
                          value={department.department_slug}
                          defaultChecked={selectedDepartment.includes(department.department_slug) ? true : false}
                          className="custom-control-input"
                          id={department.department_name}
                        />
                        <label className="custom-control-label" htmlFor={department.department_name}>
                          {" "}
                          {language === "en" ? department.department_name : department.department_name_bd}
                        </label>
                      </div>

                      <div className="collection-collapse-block-content">
                        <div className="collection-brand-filter">
                          {department.subdepartment.slice(0, 3).map((subdepartment, index) => {
                            return (
                              <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                                <input
                                  type="checkbox"
                                  onClick={e => this.clickDepartmentHandle(e, selectedDepartment)}
                                  value={subdepartment.subdepartment_slug}
                                  defaultChecked={selectedDepartment.includes(subdepartment.subdepartment_slug) ? true : false}
                                  className="custom-control-input"
                                  id={subdepartment.subdepartment_name}
                                />
                                <label className="custom-control-label" htmlFor={subdepartment.subdepartment_name}>
                                  {" "}
                                  {language === "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                                </label>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}
          </SlideToggle>
        </div>

        {/*price filter start here */}
        <SlideToggle whenReversedUseBackwardEase>
          {({ onToggle, setCollapsibleElement }) => (
            <div className="collection-collapse-block open">
              <h3 className="collapse-block-title" onClick={onToggle}>
                {language === "en" ? "price" : "মূল্য"}
              </h3>
              <div className="collection-collapse-block-content block-price-content" ref={setCollapsibleElement}>
                <div className="collection-brand-filter">
                  <div className="custom-control custom-checkbox collection-filter-checkbox">
                    <InputRange
                      formatLabel={value => {
                        if (value === -1) return "";
                        else return `${symbol}${value}`;
                      }}
                      steps={10}
                      maxValue={this.props.prices.max}
                      minValue={this.props.prices.min}
                      value={this.state.value}
                      onChange={this.onChangePriceFilter}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}
        </SlideToggle>
        {/* brands filter  */}
        <SlideToggle>
          {({ onToggle, setCollapsibleElement }) => (
            <div className="collection-collapse-block">
              <h3 className="collapse-block-title" onClick={onToggle}>
                {language === "en" ? "Brands" : "ব্রান্ডের"}
              </h3>
              <div className="collection-collapse-block-content" ref={setCollapsibleElement}>
                <div className="collection-brand-filter">
                  {this.props.brands.map((brand, index) => {
                    return (
                      <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                        <input
                          type="checkbox"
                          onClick={e => this.clickBrandHendle(e, filteredBrands)}
                          value={brand}
                          defaultChecked={filteredBrands.includes(brand) ? true : false}
                          className="custom-control-input"
                          id={brand}
                        />
                        <label className="custom-control-label" htmlFor={brand}>
                          {brand}
                        </label>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          )}
        </SlideToggle>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  brands: getBrands(state.data.products, ownProps.category),
  colors: getColors(state.data.products, ownProps.category),
  prices: getMinMaxPrice(state.data.products, ownProps.category),
  filters: state.filters,
  language: state.Intl.locale,
  symbol: state.data.symbol
});

export default connect(mapStateToProps, {
  filterBrand,
  filterColor,
  filterPrice,
  filterDepartment
})(Filter);
