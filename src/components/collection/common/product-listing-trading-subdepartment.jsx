import React, {Component} from 'react';
import { connect } from 'react-redux'
import {Link} from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroll-component';


import { getTotal, getCartProducts } from '../../../reducers'
import { addToCart, addToWishlist, addToCompare } from '../../../actions'
import {
    getMatchProduct,
    getMatchProductSubdepartments,
    getVisibleproducts,
    getVisibleproductsSubdepartments
} from '../../../services';
import ProductListItem from "./product-list-item";
import Slider from "react-slick/lib";
import {Product4} from "../../../services/script";
import ProductItem from "../../layouts/vegetables/product-item";

class ProductListingTradingSubdepartment extends Component {

    constructor (props) {
        super (props)

        this.state = { limit: 5, hasMoreItems: true };

    }

    componentWillMount(){
        this.fetchMoreItems();
    }

    fetchMoreItems = () => {
        if (this.state.limit >= this.props.products.length) {
            this.setState({ hasMoreItems: false });
            return;
        }
        // a fake async api call
        setTimeout(() => {
            this.setState({
                limit: this.state.limit + 5
            });
        }, 3000);


    }

    render (){
        const {products, addToCart, symbol, addToWishlist, addToCompare,category,incrementQty,decrementQty,removeFromCart,getMatchProduct} = this.props;
        return (
            <div style={{marginBottom:"50px"}}>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="title4">
                                <h4 className="text-xl-center">
                                    <span>Trending Now</span>
                                </h4>
                                <div className="line">
                                    <span></span>
                                </div>
                            </div>
                            <br></br>

                            {(() => {
                                if (products=='') {
                                    return (
                                        <div>{getMatchProduct.length > 0 ?
                                            <Slider {...Product4} className="product-4 category-trading col-md-12 product-m no-arrow">
                                                {getMatchProduct.map((product, index) => (
                                                    <div key={index}>
                                                        <ProductListItem
                                                            product={product}
                                                            symbol={symbol}
                                                            onAddToCompareClicked={() => addToCompare(product)}
                                                            onAddToWishlistClicked={() => addToWishlist(product)}
                                                            onAddToCartClicked={() => addToCart(product, 1)}
                                                            onIncrementClicked={() => incrementQty(product, 1)}
                                                            onDecrementClicked={() => decrementQty(product.id)}
                                                            onRemoveFromCart={() => removeFromCart(product)}
                                                            key={index}
                                                        />
                                                    </div>
                                                ))}
                                            </Slider>
                                            :
                                            <div className="row">
                                                <div className="col-sm-12 text-center section-b-space mt-5 no-found" >
                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/empty-search.jpg`} className="img-fluid mb-4" />
                                                    <h3>Sorry! Couldn't find the product you were looking For!!!    </h3>
                                                    <p>Please check if you have misspelt something or try searching with other words.</p>
                                                    <Link to={`${process.env.PUBLIC_URL}/`} className="btn btn-solid">continue shopping</Link>
                                                </div>
                                            </div>
                                        }</div>
                                    )
                                }else {
                                    return (
                                        <div>{products.length > 0 ?
                                            <Slider {...Product4} className="product-4 category-trading col-md-12 product-m no-arrow">
                                                {products.map((product, index) => (
                                                    <div key={index}>
                                                        <ProductListItem
                                                            product={product}
                                                            symbol={symbol}
                                                            onAddToCompareClicked={() => addToCompare(product)}
                                                            onAddToWishlistClicked={() => addToWishlist(product)}
                                                            onAddToCartClicked={() => addToCart(product, 1)}
                                                            onIncrementClicked={() => incrementQty(product, 1)}
                                                            onDecrementClicked={() => decrementQty(product.id)}
                                                            onRemoveFromCart={() => removeFromCart(product)}
                                                            key={index}
                                                        />
                                                    </div>
                                                ))}
                                            </Slider>
                                            :
                                            <div className="row">
                                                <div className="col-sm-12 text-center section-b-space mt-5 no-found" >
                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/empty-search.jpg`} className="img-fluid mb-4" />
                                                    <h3>Sorry! Couldn't find the product you were looking For!!!    </h3>
                                                    <p>Please check if you have misspelt something or try searching with other words.</p>
                                                    <Link to={`${process.env.PUBLIC_URL}/`} className="btn btn-solid">continue shopping</Link>
                                                </div>
                                            </div>
                                        }</div>
                                    )
                                }
                            })()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state,ownProps) => ({
    products: getVisibleproductsSubdepartments(state.data, state.filters,ownProps.category),
    getMatchProduct: getMatchProductSubdepartments(state.data, state.filters,ownProps.category),
    symbol: state.data.symbol,
})

export default connect(
    mapStateToProps, {addToCart, addToWishlist, addToCompare}
)(ProductListingTradingSubdepartment)