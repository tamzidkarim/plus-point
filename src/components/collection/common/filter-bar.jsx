import React, { Component } from "react";
import { connect } from "react-redux";
import { filterSort } from "../../../actions";
import { getVisibleproducts } from "../../../services";

class FilterBar extends Component {
  constructor(props) {
    super(props);

    this.state = { limit: 5, hasMoreItems: true };
  }

  componentWillMount() {
    this.fetchMoreItems();
  }

  fetchMoreItems = () => {
    if (this.state.limit >= this.props.products.length) {
      this.setState({ hasMoreItems: false });
      return;
    }
    // a fake async api call
    setTimeout(() => {
      this.setState({
        limit: this.state.limit + 5
      });
    }, 3000);
  };

  //List Layout View
  listLayout() {
    document.querySelector(".collection-grid-view").style = "opacity:0";
    document.querySelector(".product-wrapper-grid").style = "opacity:0.2";
    document.querySelector(".product-wrapper-grid").classList.add("list-view");
    var elems = document.querySelector(".infinite-scroll-component .row").childNodes;
    [].forEach.call(elems, function(el) {
      el.className = "";
      el.classList.add("col-lg-12");
    });
    setTimeout(function() {
      document.querySelector(".product-wrapper-grid").style = "opacity: 1";
    }, 500);
  }

  //Grid Layout View
  gridLayout() {
    document.querySelector(".collection-grid-view").style = "opacity:1";
    document.querySelector(".product-wrapper-grid").classList.remove("list-view");
    var elems = document.querySelector(".infinite-scroll-component .row").childNodes;
    [].forEach.call(elems, function(el) {
      el.className = "";
      el.classList.add("col-lg-3");
    });
  }
  openFilter = () => {
    document.querySelector(".collection-filter").style = "left: 0px";
  };

  // Layout Column View
  LayoutView = colSize => {
    if (!document.querySelector(".product-wrapper-grid").classList.contains("list-view")) {
      var elems = document.querySelector(".infinite-scroll-component .row").childNodes;
      [].forEach.call(elems, function(el) {
        el.className = "";
        el.classList.add("col-lg-" + colSize);
      });
    }

    this.props.onLayoutViewClicked(colSize);
  };

  render() {
    const { category, language } = this.props;
    var title = "";
    if (category[0] == "top") {
      title = language == "en" ? "TOP" : "টপ";
    } else if (category[0] == "bottom") {
      title = language == "en" ? "BOTTOM" : "বটম";
    } else if (category[0] == "footwear") {
      title = language == "en" ? "FOOTWEAR" : "জুতা";
    }
    return (
      <div className="">
        <div className="">
          <div className="page-title">
            <h3>{title}</h3>
          </div>
          <div className="collection-mobile-back">
            <span className="filter-back" onClick={e => this.openFilter(e)}>
              <img src={`${process.env.PUBLIC_URL}/assets/images/filter.png`} className="img-fluid" alt="" />
            </span>
          </div>
        </div>

        <div className="product-filter-content">
          <div className="product-page-filter">
            <select onChange={e => this.props.filterSort(e.target.value)}>
              <option value="">{language == "en" ? "Sorting items" : "আইটেম বাছাই"}</option>
              <option value="HighToLow">{language == "en" ? "Price: High to Low" : "দাম: উচ্চ থেকে কম"}</option>
              <option value="LowToHigh">{language == "en" ? "Price: Low to High" : "দাম: কম থেকে উচ্চ"}</option>
              <option value="Newest">{language == "en" ? "Newest Items" : "নতুন আইটেম"}</option>
              <option value="AscOrder">{language == "en" ? "Sort By Name: A To Z" : "নাম অনুসারে বাছাই করুন:এ টু জেড"}</option>
              <option value="DescOrder">{language == "en" ? "Sort By Name: Z To A" : "নাম অনুসারে বাছাই করুন:জেড টু এ"}</option>
            </select>
          </div>
          <div className="search-count">
            <h5>
              {language == "en" ? "Showing Products 1-" : "পণ্যগুলি দেখানো হচ্ছে 1-"}
              {this.props.products.length} {language == "en" ? "ofu" : "এর"}
              {this.props.products.length} {language == "en" ? "results" : "ফলাফল"}
            </h5>
          </div>
          <div className="show-count">
            <div className="product-page-filter1">
              <select onChange={e => this.props.filterSort(e.target.value)}>
                <option value="6">{language == "en" ? "Show: 6 " : "দেখান: 6"}</option>
                <option value="12"> {language == "en" ? "Show: 12" : "দেখান: 12"}</option>
                <option value="18"> {language == "en" ? "Show: 18" : "দেখান: 18"}</option>
                <option value="24"> {language == "en" ? "Show: 24" : "দেখান: 24"}</option>
              </select>
            </div>
          </div>
          <div className="collection-view">
            <ul>
              <li>
                <i className="fa fa-th-large grid-layout-view" aria-hidden="true" onClick={() => this.LayoutView(6)}></i>
              </li>
              <li>
                <i className="fa fa-th grid-layout-view" aria-hidden="true" onClick={() => this.LayoutView(4)}></i>
              </li>
              <li>
                <i className="fa fa-th grid-layout-view" aria-hidden="true" onClick={() => this.LayoutView(3)}></i>
              </li>
            </ul>
          </div>
        </div>

        {/* <div className="collection-grid-view">
          <ul>
            <li>
              <img
                src={`${process.env.PUBLIC_URL}/assets/images/icon/2.png`}
                alt=""
                className="product-2-layout-view"
                onClick={() => this.LayoutView(6)}
              />
            </li>
            <li>
              <img
                src={`${process.env.PUBLIC_URL}/assets/images/icon/3.png`}
                alt=""
                className="product-3-layout-view"
                onClick={() => this.LayoutView(4)}
              />
            </li>
            <li>
              <img
                src={`${process.env.PUBLIC_URL}/assets/images/icon/4.png`}
                alt=""
                className="product-4-layout-view"
                onClick={() => this.LayoutView(3)}
              />
            </li>
            <li>
              <img
                src={`${process.env.PUBLIC_URL}/assets/images/icon/6.png`}
                alt=""
                className="product-6-layout-view"
                onClick={() => this.LayoutView(2)}
              />
            </li>
          </ul>
        </div> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: getVisibleproducts(state.data, state.filters),
  filters: state.filters,
  language: state.Intl.locale
});

export default connect(mapStateToProps, { filterSort })(FilterBar);
