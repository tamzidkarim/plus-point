import React, { Component } from "react";
import { Link } from "react-router-dom";
import Modal from "react-responsive-modal";
import { connect } from "react-redux";

class ProductListItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      stock: "InStock",
      quantity: 1,
      image: ""
    };
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  onClickHandle(img) {
    this.setState({ image: img });
  }

  minusQty = () => {
    if (this.state.quantity > 1) {
      this.setState({ stock: "InStock" });
      this.setState({ quantity: this.state.quantity - 1 });
    }
  };

  plusQty = () => {
    if (this.props.product.stock >= this.state.quantity) {
      this.setState({ quantity: this.state.quantity + 1 });
    } else {
      this.setState({ stock: "Out of Stock !" });
    }
  };
  changeQty = e => {
    this.setState({ quantity: parseInt(e.target.value) });
  };

  loadProductPreviewImages(product) {
    if (product.imageSize.length == 0) {
      return <img src={`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />;
    } else {
      let items = product.imageSize.slice(0, 2).map((image, index) => {
        if (index == 0) {
          return <img src={`${image.large}`} alt="" className="img-top" />;
        } else {
          return <img src={`${image.large}`} alt="" />;
        }
      });
      return items;
    }
  }

  render() {
    const { product, symbol, onAddToCartClicked, onAddToWishlistClicked, onAddToCompareClicked, language } = this.props;
    const { open } = this.state;

    let RatingStars = [];
    for (var i = 0; i < product.rating; i++) {
      RatingStars.push(<i className="fa fa-star" key={i}></i>);
    }

    return (
      <div>
        <div className="plus-point-tab-item">
          <div className="imageBox">
            <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`}>{this.loadProductPreviewImages(product)}</Link>
          </div>
          <p>{language == "en" ? product.subDepartmentName : product.subDepartmentName_bd}</p>
          <a className="product-item-title" href="#!">
            {language == "en" ? product.name : product.name_bd}
          </a>
          <br></br>
          <span className="old-price">
            <del>
              {symbol}
              {product.price}
            </del>
          </span>{" "}
          <span className="new-price">
            {symbol}
            {product.salePrice}
          </span>
          <div className="product-right">
            <div>
              <div className="size-box">
                <ul>
                  <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                    <button>M</button>
                  </li>
                  <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                    <button>L</button>
                  </li>
                  <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                    <button>XL</button>
                  </li>
                  <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                    <button>L</button>
                  </li>
                </ul>
              </div>
            </div>
            <div>
              <div className="size-box">
                <ul>
                  <li style={{ background: "blue", width: "30px", height: "30px" }}>
                    <button></button>
                  </li>
                  <li style={{ background: "black", width: "30px", height: "30px" }}>
                    <button></button>
                  </li>
                  <li style={{ background: "red", width: "30px", height: "30px" }}>
                    <button></button>
                  </li>
                  <li style={{ background: "Green", width: "30px", height: "30px" }}>
                    <button></button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <ul>
            <li>
              <button className="item-add-to-cart-btn" onClick={() => onAddToCartClicked(product, this.state.quantity)}>
                ADD TO CART
              </button>
            </li>
            <li>
              <button onClick={onAddToWishlistClicked}>
                <img className="whish-icon" src={`${process.env.PUBLIC_URL}/assets/images/whish-icon.png`} alt="" />
              </button>
            </li>
          </ul>
          <span>{RatingStars}</span>
        </div>
        <Modal open={this.state.open} onClose={this.onCloseModal} center>
          <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div className="modal-content quick-view-modal">
              <div className="modal-body">
                <div className="row">
                  <div className="col-lg-6  col-xs-12">
                    <div className="quick-view-img">
                      <img
                        src={`${
                          product.variants ? (this.state.image ? this.state.image : product.variants[0].images) : product.pictures[0]
                        }`}
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                  <div className="col-lg-6 rtl-text">
                    <div className="product-right">
                      <h2> {product.name} </h2>
                      <h3>
                        {symbol}
                        {product.price}
                      </h3>
                      {product.variants ? (
                        <ul className="color-variant">
                          {product.variants.map((vari, i) => (
                            <li className={vari.color} key={i} title={vari.color} onClick={() => this.onClickHandle(vari.images)}></li>
                          ))}
                        </ul>
                      ) : (
                        ""
                      )}
                      <div className="border-product">
                        <h6 className="product-title">product details</h6>
                        <p>{product.shortDetails}</p>
                      </div>
                      <div className="product-description border-product">
                        {product.size ? (
                          <div className="size-box">
                            <ul>
                              {product.size.map((size, i) => {
                                return (
                                  <li key={i}>
                                    <a href="#">{size}</a>
                                  </li>
                                );
                              })}
                            </ul>
                          </div>
                        ) : (
                          ""
                        )}
                        <h6 className="product-title">quantity</h6>
                        <div className="qty-box">
                          <div className="input-group">
                            <span className="input-group-prepend">
                              <button
                                type="button"
                                className="btn quantity-left-minus"
                                onClick={this.minusQty}
                                data-type="minus"
                                data-field=""
                              >
                                <i className="fa fa-angle-left"></i>
                              </button>
                            </span>
                            <input
                              type="text"
                              name="quantity"
                              value={this.state.quantity}
                              onChange={this.changeQty}
                              className="form-control input-number"
                            />
                            <span className="input-group-prepend">
                              <button
                                type="button"
                                className="btn quantity-right-plus"
                                onClick={this.plusQty}
                                data-type="plus"
                                data-field=""
                              >
                                <i className="fa fa-angle-plus"></i>
                              </button>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="product-buttons">
                        <button className="btn btn-solid" onClick={() => onAddToCartClicked(product, this.state.quantity)}>
                          add to cart
                        </button>
                        <Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`} className="btn btn-solid">
                          view detail
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps)(ProductListItem);
