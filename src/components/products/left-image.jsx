import React, { Component } from "react";
import Slider from "react-slick";
import "../common/index.scss";
import { connect } from "react-redux";
import ReactImageMagnify from "react-image-magnify";

// import custom Components
import RelatedProduct from "../common/related-product";
import Breadcrumb from "../common/breadcrumb";
import DetailsWithPrice from "./common/product/details-price";
import DetailsTopTabs from "./common/details-top-tabs";
import { addToCart, addToCartUnsafe, addToWishlist } from "../../actions";
import image1 from "./02.png";

class LeftImage extends Component {
  constructor() {
    super();
    this.state = {
      nav1: null,
      nav2: null,
      vertical: true,
      image: image1,
      singleImage: false,
      secondImage: ""
    };
  }

  componentWillMount() {
    if (window.innerWidth > 576) {
      this.setState({ vertical: true });
    } else {
      this.setState({ vertical: false });
    }
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  render() {
    const { item, addToCart, addToCartUnsafe, addToWishlist } = this.props;

    console.log("TTTTT", item);
    // var products = {
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   arrows: true,
    //   fade: true
    // };
    var productsnav = {
      vertical: this.state.vertical,
      verticalSwiping: this.state.vertical,
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: ".product-right-slick",
      arrows: false,
      infinite: true,
      centerMode: false,
      dots: false,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }
      ]
    };

    return (
      <div>
        {item ? <Breadcrumb parent={"Product"} title={item.name} /> : ""}

        {/*Section Start*/}
        {item ? (
          <section>
            <div className="collection-wrapper">
              <div className="container">
                <div className="row">
                  <div className="col-lg-1 col-sm-2 col-xs-12 p-0">
                    {/* <SmallImages item={item} settings={productsnav} navOne={this.state.nav1} /> */}
                    <div className="row">
                      <div className="col-12 p-0">
                        <Slider
                          {...productsnav}
                          asNavFor={this.props.navOne}
                          ref={slider => (this.slider2 = slider)}
                          className="slider-nav"
                        >
                          {item.variants
                            ? item.variants.map((vari, index) => (
                                <div key={index}>
                                  <img src={`${vari.images}`} key={index} alt="" className="img-fluid" />
                                </div>
                              ))
                            : item.pictures.map((vari, index) => (
                                <div key={index}>
                                  <img
                                    onClick={() => {
                                      this.setState({ secondImage: vari });
                                    }}
                                    src={`${vari}`}
                                    key={index}
                                    alt=""
                                    className="img-fluid"
                                  />
                                </div>
                              ))}
                        </Slider>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-5 col-sm-10 col-xs-12  order-up">
                    {/* <Slider {...products} asNavFor={this.state.nav2} ref={slider => (this.slider1 = slider)} className="product-slick">
                      {item.variants
                        ? item.variants.map((vari, index) => (
                            <div key={index}>
                              <ImageZoom image={vari.images} />
                            </div>
                          ))
                        : item.imageSize.map((vari, index) => (
                            <div key={index}>
                              <ImageZoom image={vari.large} />
                            </div>
                          ))}
                    </Slider> */}
                    {item && (
                      <ReactImageMagnify
                        {...{
                          smallImage: {
                            alt: "",
                            isFluidWidth: true,
                            src: this.state.secondImage.length < 1 ? item.imageSize[0].large : this.state.secondImage
                          },
                          largeImage: {
                            src: this.state.secondImage.length < 1 ? item.imageSize[0].large : this.state.secondImage,
                            width: 1200,
                            height: 1800
                          },
                          enlargedImagePosition: "over"
                        }}
                      />
                    )}
                  </div>
                  <DetailsWithPrice
                    item={item}
                    navOne={this.state.nav1}
                    addToCartClicked={addToCart}
                    BuynowClicked={addToCartUnsafe}
                    addToWishlistClicked={addToWishlist}
                  />
                </div>
              </div>
            </div>
          </section>
        ) : (
          ""
        )}
        {/*Section End*/}

        {/* Review Section starts */}
        {item ? (
          <section className="tab-product m-0">
            <div className="container">
              <div className="row">
                <div className="col-sm-12 col-lg-12">
                  <DetailsTopTabs item={item} />
                </div>
              </div>
            </div>
          </section>
        ) : (
          ""
        )}
        {/* Review Section ends */}
        <RelatedProduct />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let productId = ownProps.match.params.id;
  return {
    item: state.data.products.find(el => el.id === productId),
    symbol: state.data.symbol
  };
};

export default connect(mapStateToProps, {
  addToCart,
  addToCartUnsafe,
  addToWishlist
})(LeftImage);
