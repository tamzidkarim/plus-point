import React, { Component } from "react";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { addToCart } from "../../../../actions";
import _ from "lodash";

import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  EmailShareButton,
  EmailIcon,
  LinkedinShareButton,
  LinkedinIcon
} from "react-share";

class DetailsWithPrice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      size: "",
      color: "",
      stockStatus: "InStock",
      filteredCurrentStock: null,
      nav3: null,
      reviewCount: null,
      uniqueColors: [],
      uniqueSize: []
    };
  }

  componentDidMount() {
    const { item } = this.props;

    if (item.variation_type === 2) {
      this.setState({ color: item.variation[0].color });
      this.setState({ size: item.variation[0].size });
      const uniqueColors = _.uniqBy(item.variation, "color").map(item => {
        return item.color;
      });
      const uniqueSize = _.uniqBy(item.variation, "size").map(item => {
        return item.size;
      });
      this.setState({ uniqueColors }, this.setState({ uniqueSize }, this.calculateCurrentStock));
    } else {
      this.setState({ filteredCurrentStock: item.stock });
    }

    this.setState({
      nav3: this.slider3
    });
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/item-reviews?access-token=9e74904aa7581d08c1465f8fc842630c", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    })
      .then(resp => resp.json())
      .then(response => {
        const productReview = response.data.filter(review => this.props.item.id == review.itemId);
        this.setState({ reviewCount: productReview.length });
      });
  }

  minusQty = () => {
    if (this.state.quantity > 1) {
      this.setState({ stockStatus: "InStock" });
      this.setState({ quantity: this.state.quantity - 1 });
    }
  };

  plusQty = () => {
    if (this.props.item.stock >= this.state.quantity) {
      this.setState({ quantity: this.state.quantity + 1 });
    } else {
      this.setState({ stockStatus: "Out of Stock !" });
    }
  };

  changeQty = e => {
    this.setState({ quantity: parseInt(e.target.value) });
  };

  handleAddToCart = event => {
    let { item } = this.props;
    let { color, size, quantity, filteredCurrentStock } = this.state;

    // if (item.colors.length == 0 || item.colors[0].length == 0) {
    //   color = "default";
    // }
    // if (color === null || color === "") {
    //   alert("You must choose a color");
    // } else if (size === null || size === "") {
    //   alert("You must choose a size");
    // } else {
    //   this.props.addToCart(item, quantity, color, size);
    // }
    let variation;
    if (filteredCurrentStock - quantity >= 0) {
      if (item.variation_type != 2) {
        this.props.addToCart(item, quantity);
      } else {
        variation = item.variation.find(item => {
          if (item.color == color && item.size == size) {
            return item;
          }
        });
        console.log("sdafasdf", variation);
        addToCart(item, quantity, color, size, variation.id);
      }
    } else {
      alert("Not enough items in Stock!!");
    }
  };

  calculateCurrentStock = () => {
    const { color, size } = this.state;
    const { item } = this.props;
    if (color === "" && size === "") {
      const totalStock = item.variation.reduce((sum, item) => {
        return sum + item.quantity;
      }, 0);
      this.setState({ filteredCurrentStock: totalStock });
      return;
    } else {
      const totalStock = item.variation.reduce((sum, item) => {
        if (item.color == (this.state.color || "") && item.size == (this.state.size || "")) {
          return sum + item.quantity;
        } else {
          return sum;
        }
      }, 0);
      this.setState({ filteredCurrentStock: totalStock });
    }
  };

  // handleSizeClick = (e) => {
  //   this.setState({size: e.target.value});
  //   if()
  //   const temp = document.getElementById("test");
  //   temp.classList.add("addDiv");
  // }

  render() {
    const { symbol, item, addToCartClicked, BuynowClicked, addToWishlistClicked, language } = this.props;
    const { reviewCount, uniqueSize, uniqueColors, filteredCurrentStock } = this.state;
    var colorsnav = {
      slidesToShow: 6,
      swipeToSlide: true,
      arrows: false,
      dots: false,
      focusOnSelect: true
    };

    return (
      <div className="col-lg-6 rtl-text">
        <Helmet>
          <title>
            Pluspoint | {item.category} | {item.name}
          </title>
          <meta property="og:url" content={`http://ecom2.unlockretail.com/left-sidebar/product/${item.id}`} />
          <meta property="og:description" content={item.description ? item.description : "blank_description"} />
          <meta property="og:image" content={item.pictures[0] ? item.pictures[0] : ""} />
          <meta property="fb:app_id" content="1042242282782239" />
        </Helmet>
        <div className="product-right">
          <span style={{ background: "red", marginRight: "5px" }} className="in-stock-btn">
            -{item.discount}%
          </span>
          <span className="in-stock-btn">{this.state.stockStatus}</span>
          <h2> {language == "en" ? item.name : item.name_bd} </h2>
          <div className="single-page-rating">
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            {reviewCount && (
              <span>
                {" "}
                {reviewCount} {reviewCount > 1 ? "Reviews" : "Review"}
              </span>
            )}
          </div>
          <div className="all-price">
            <del>
              {symbol}
              {item.salePrice}
            </del>{" "}
            <h3>
              {symbol}
              {item.salePrice - (item.salePrice * item.discount) / 100}{" "}
            </h3>
          </div>
          <div className="save-percentage">
            {language == "en" ? "You Save:" : "আপনি সংরক্ষণ করুন:"}{" "}
            <span>
              {" "}
              {symbol}
              {(item.salePrice * item.discount) / 100}
            </span>
            <span> ({item.discount}% off)</span>
          </div>
          <div className="product-description border-product">
            {item.variation_type === 2 ? (
              <div>
                <h6 className="product-title size-text">{language == "en" ? "Size" : "আকার"}</h6>

                <div className="size-box">
                  <ul>
                    {uniqueSize.map((s, i) => {
                      if (s.toString() == this.state.size) {
                        return (
                          <li
                            value={s.toString()}
                            onClick={e => {
                              this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            style={{ border: "2px solid #fff", borderRadius: "0", boxShadow: "0 0 0 3px hsl(0, 0%, 80%)" }}
                            key={i}
                            id={i}
                          >
                            {s}
                          </li>
                        );
                      } else {
                        return (
                          <li
                            value={s.toString()}
                            onClick={e => {
                              this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            style={{ border: "1px solid #000", borderRadius: "0" }}
                            key={i}
                            id={i}
                          >
                            {s}
                          </li>
                        );
                      }
                    })}
                  </ul>
                </div>
              </div>
            ) : (
              ""
            )}
            <button type="button" className="btn size-btn" data-toggle="modal" data-target="#exampleModalCenter">
              {language == "en" ? "Size Chart" : "মাপের তালিকা"}
            </button>
            <div>
              {item.colors[0].length > 0 && <h6 className="product-title size-text">{language == "en" ? "Color" : "রং"}</h6>}
              <div className="size-box">
                <ul>
                  {uniqueColors.map(color => {
                    if (color.length > 1) {
                      if (color == this.state.color) {
                        return (
                          <li
                            value={color.toString()}
                            onClick={e => {
                              console.log(e.currentTarget.getAttribute("value"));
                              this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            style={{
                              background: `${color}`,
                              width: "20px",
                              height: "20px",
                              border: "3px solid #fff",
                              boxShadow: "0 0 0 3px hsl(0, 0%, 80%)"
                            }}
                          ></li>
                        );
                      } else {
                        return (
                          <li
                            value={color.toString()}
                            onClick={e => {
                              console.log(e.currentTarget.getAttribute("value"));
                              this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            style={{ background: `${color}`, width: "20px", height: "20px", border: "1px solid #000" }}
                          ></li>
                        );
                      }
                    }
                  })}
                </ul>
              </div>
            </div>
            <div
              className="modal fade"
              id="exampleModalCenter"
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalCenterTitle"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">
                      {language == "en" ? "Size Chart" : "মাপের তালিকা"}
                    </h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <img src={`${process.env.PUBLIC_URL}/assets/images/size-chart.png`} alt="" className="img-fluid" />
                  </div>
                </div>
              </div>
            </div>
            <h6 className="product-title">{language == "en" ? "quantity" : "পরিমাণ"}</h6>
            <div className="qty-box">
              <div className="input-group">
                <span className="input-group-prepend">
                  <button type="button" className="btn quantity-left-minus" onClick={this.minusQty} data-type="minus" data-field="">
                    <i className="fa fa-minus"></i>
                  </button>
                </span>
                <input
                  type="text"
                  name="quantity"
                  value={this.state.quantity}
                  onChange={this.changeQty}
                  className="form-control input-number"
                />
                <span className="input-group-prepend">
                  <button type="button" className="btn quantity-right-plus" onClick={this.plusQty} data-type="plus" data-field="">
                    <i className="fa fa-plus"></i>
                  </button>
                </span>
              </div>
            </div>
            <p style={{ margin: "10px 0" }}>
              {language == "en" ? "Hurry! Only" : "তাড়াতাড়ি! কেবল"}{" "}
              <span style={{ color: "red", fontWeight: "bold" }}>{filteredCurrentStock}</span>
              {language == "en" ? " Left in Stock!" : "স্টক মধ্যে বাকি!"}
            </p>
            <div className="progress">
              <div
                className="progress-bar"
                role="progressbar"
                style={{ width: `${filteredCurrentStock}%` }}
                aria-valuenow={item.stock}
                aria-valuemin="0"
                aria-valuemax="100"
              ></div>
            </div>
            <p style={{ marginTop: "10px" }}>
              {language == "en"
                ? "Order in the next 24 Hours to get it by Tuesday 17/2/2020"
                : "মঙ্গলবার 17/2/2020 এর মধ্যে পরবর্তী 24 ঘন্টা অর্ডার করুন"}
            </p>
          </div>
          <div className="product-buttons">
            <div className="row">
              <div className="col-md-12">
                <a className="btn btn-solid btn-cart " onClick={this.handleAddToCart}>
                  {language == "en" ? "add to cart" : "কার্ট যোগ করুন"}
                </a>
              </div>
              <div className="col-md-6">
                <a className="btn btn-solid btn-wishlist" onClick={() => addToWishlistClicked(item)}>
                  {language == "en" ? "add to wishlist" : "চাহিদাপত্রে যোগ করা"}
                </a>
              </div>
              <div className="col-md-6">
                <a className="btn btn-solid btn-compare" onClick={() => addToCartClicked(item, this.state.quantity)}>
                  {language == "en" ? "add to compare" : "তুলনা যোগ করুন"}
                </a>
              </div>
              <div className="col-md-12">
                <Link
                  to={`${process.env.PUBLIC_URL}/checkout`}
                  className="btn btn-solid btn-buy"
                  onClick={() => BuynowClicked(item, this.state.quantity)}
                >
                  {language == "en" ? "buy now" : "এখন কিনুন"}
                </Link>
              </div>
              <div className="col-md-12">
                <img className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/payment-getwaye.png`} alt="user" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale,
  symbol: state.data.symbol
});

export default connect(mapStateToProps, { addToCart })(DetailsWithPrice);
