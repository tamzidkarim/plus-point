import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.scss";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Breadcrumb from "../../common/breadcrumb";
import _ from "lodash";
import axios from "axios";
import connect from "react-redux/lib/connect/connect";

class DetailsTopTabs extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      token: "",
      ReviewSubject: "",
      ReviewRating: "",
      ReviewText: "",
      product_id: "",
      author_id: "",
      error_subject: "",
      error_rating: "",
      error_text: "",
      ratingItem: []
    };
    this.ReviewSubject = this.ReviewSubject.bind(this);
    this.ReviewRating = this.ReviewRating.bind(this);
    this.ReviewText = this.ReviewText.bind(this);
    this.RegistrationReview = this.RegistrationReview.bind(this);
  }
  componentWillMount() {
    const tokenVar = localStorage.getItem("token");
    const author_id = localStorage.getItem("author_id");
    this.setState({
      token: tokenVar,
      author_id: author_id,
      product_id: this.props.item.id
    });
    this.getRatingList(this.props.item.id);
  }

  ReviewSubject(event) {
    this.setState({ ReviewSubject: event.target.value });
  }
  ReviewRating(event) {
    this.setState({ ReviewRating: event.target.value });
  }
  ReviewText(event) {
    this.setState({ ReviewText: event.target.value });
  }

  RegistrationReview(event) {
    const data = new FormData();

    data.append("ItemReview[name]", this.state.ReviewSubject);
    data.append("ItemReview[comment]", this.state.ReviewText);
    data.append("ItemReview[rating]", this.state.ReviewRating);
    data.append("ItemReview[itemId]", this.state.product_id);

    const headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "content-type": "multipart/form-data",
      "X-Requested-With": "XMLHttpRequest"
    };

    axios
      .post("https://pluspointapi.unlockretail.com/api/web/v1/item-reviews?access-token=9e74904aa7581d08c1465f8fc842630c", data, {
        headers: headers
      })
      .then(response => {
        if (response.data.success) {
          toast.success("Your review has been added successfully");
          this.setState({ ratingItem: [...this.state.ratingItem, response.data.data] });
          this.setState({ ReviewSubject: "", ReviewText: "", ReviewRating: "" });
        }
      });
    // fetch("http://pluspointapi.unlockretail.com/api/web/v1/item-reviews?access-token=9e74904aa7581d08c1465f8fc842630c", {
    //   method: "post",
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json",
    //     "X-Requested-With": "XMLHttpRequest"
    //   },
    //   body: JSON.stringify({
    //     name: this.state.ReviewSubject,
    //     rating: this.state.ReviewRating,
    //     comment: this.state.ReviewText,
    //     itemId: this.state.product_id
    //   })
    // })
    //   .then(Response => Response.json())
    //   .then(result => {
    //     console.log(result)
    //     if (result.status == "401") {
    //       this.setState({ error_subject: result.errors.review_subject ? result.errors.review_subject : "" });
    //       this.setState({ error_rating: result.errors.review_rating ? result.errors.review_rating : "" });
    //       this.setState({ error_text: result.errors.review_text ? result.errors.review_text : "" });
    //     } else {
    //       event.preventDefault();
    //       this.setState({ ReviewSubject: "", ReviewText: "", ReviewRating: "" });
    //       etoast.success("Your review has ben added successfully");
    //     }
    //   });
  }

  getRatingList(product_id) {
    if (product_id) {
      fetch("https://pluspointapi.unlockretail.com/api/web/v1/item-reviews?access-token=9e74904aa7581d08c1465f8fc842630c", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        }
      })
        .then(resp => resp.json())
        .then(response => {
          const productReview = response.data.filter(review => this.state.product_id == review.itemId);
          this.setState({
            ratingItem: productReview
          });
        });
    }
  }

  render() {
    const { item, language } = this.props;
    const { token, author_id, ratingItem } = this.state; //User Login Token

    let RatingStars = [];
    for (var i = 0; i < item.rating; i++) {
      RatingStars.push(<i className="fa fa-star" key={i}></i>);
    }

    return (
      <section className="tab-product m-0">
        <div className="row">
          <div className="col-sm-12 col-lg-12">
            <Tabs className="tab-content nav-material">
              <TabList className="nav nav-tabs nav-material">
                <Tab className="nav-item">
                  <span className="nav-link">
                    <i className="icofont icofont-contacts"></i>
                    {language === "en" ? "Description" : "বিবরণ"}
                  </span>
                  <div className="material-border"></div>
                </Tab>
                <Tab className="nav-item">
                  <span className="nav-link">
                    <i className="icofont icofont-contacts"></i>
                    {language === "en" ? "Additional" : "উপরন্তু"}
                  </span>
                  <div className="material-border"></div>
                </Tab>
                <Tab className="nav-item">
                  <span className="nav-link active">
                    <i className="icofont icofont-contacts"></i>
                    {language === "en" ? "Write Review" : "রিভিউ লিখুন"}
                  </span>
                  <div className="material-border"></div>
                </Tab>
              </TabList>
              <TabPanel>
                <p className="mt-4 p-0">{item.description}</p>
              </TabPanel>
              <TabPanel>
                <p className="mt-4 p-0">{item.shortDetails}</p>
              </TabPanel>
              <TabPanel>
                <form className="theme-form mt-4">
                  <div className="form-row">
                    <div className="col-md-12 ">
                      <div className="media m-0">
                        <label>{language === "en" ? "Rating" : "রেটিং"}</label>
                        <div className="media-body ml-3">
                          <div className="rating three-star">{RatingStars}</div>
                        </div>
                      </div>
                    </div>
                    {ratingItem.length > 0 ? (
                      <div className="col-md-12">
                        {ratingItem.map((item, index) => {
                          return (
                            <div className="comment-body" key={index}>
                              <div className="card">
                                <div className="card-body">
                                  <div className="row">
                                    <div className="col-md-1">
                                      <img src={item.author_image} className="img img-rounded img-fluid" />
                                    </div>
                                    <div className="col-md-11">
                                      <p>
                                        <strong>{item.name}</strong>
                                        {_.times(item.rating, i => (
                                          <span className="float-right" key={i}>
                                            <i className="text-warning fa fa-star"></i>
                                          </span>
                                        ))}
                                      </p>
                                      <div className="clearfix"></div>
                                      <p className="text-secondary float-left created-at">
                                        <i className="fa fa-calendar"></i> {item.crAt.split(" ")[0]}
                                      </p>
                                      <br />
                                      <p className="float-left content-body">{item.comment ? item.comment : ""}</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </form>

                {/* Write Review Section Here */}

                {token ? (
                  <form className="theme-form mt-4">
                    <div className="form-row">
                      <div className="col-md-6 mg-bottom-10">
                        <label htmlFor="name">
                          {language === "en" ? "image" : "সাবজেক্ট"}
                          <span className="error_rating">*</span>
                        </label>
                        <input
                          type="text"
                          onChange={this.ReviewSubject}
                          className="form-control"
                          id="ReviewSubject"
                          placeholder="Enter Subject"
                          value={this.state.ReviewSubject}
                          required
                        />
                        <span className="error_rating">{this.state.error_subject}</span>
                      </div>
                      <div className="col-md-6">
                        <label htmlFor="email">
                          {language === "en" ? "Rating" : "রেটিং"}
                          <span className="error_rating">*</span>
                        </label>
                        <select className="form-control " value={this.state.ReviewRating} id="review_rating" onChange={this.ReviewRating}>
                          <option selected="selected" value="">
                            {language === "en" ? "Select Rating ..." : "রেটিং নির্বাচন করুন ..."}
                          </option>
                          <option value="5">{language === "en" ? "5 Stars" : "5 তারা"}</option>
                          <option value="4">{language === "en" ? "4 Stars" : "4 তারা"}</option>
                          <option value="3">{language === "en" ? "3 Stars" : "3 তারা"}</option>
                          <option value="2">{language === "en" ? "2 Stars" : "2 তারা"}</option>
                          <option value="1">{language === "en" ? "1 Stars" : "1 তারা"}</option>
                        </select>
                        <span className="error_rating">{this.state.error_rating}</span>
                      </div>
                      <div className="col-md-12">
                        <label htmlFor="review">
                          {language === "en" ? "Review Title" : "শিরোনাম পর্যালোচনা"}
                          <span className="error_rating">*</span>
                        </label>
                        <textarea
                          onChange={this.ReviewText}
                          className="form-control "
                          placeholder="Wrire Your Review About Product"
                          id="ReviewText"
                          rows="6"
                          value={this.state.ReviewText}
                        ></textarea>
                        <span className="error_rating">{this.state.error_text}</span>
                      </div>
                      <div className="col-md-12">
                        <a className="btn btn-solid review-butoon" onClick={this.RegistrationReview}>
                          {language === "en" ? "Submit Your Review" : "আপনার পর্যালোচনা জমা দিন"}
                        </a>
                      </div>
                    </div>
                  </form>
                ) : (
                  <div className="alert alert-info alert-dismissible fade show text-center margin-bottom-1x">
                    <span className="alert-close" data-dismiss="alert"></span>
                    <i className="icon-layers"></i>
                    {language === "en"
                      ? "You need to login to be able to leave a review."
                      : "একটি পর্যালোচনা ছাড়ার জন্য আপনাকে লগইন করতে হবে।"}
                  </div>
                )}
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </section>
    );
  }
}
const mapStateToProps = state => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps)(DetailsTopTabs);
