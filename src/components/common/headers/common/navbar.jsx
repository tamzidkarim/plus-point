import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import departments_json from "../../../../constants/departments.json";

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      navClose: { right: "0px" },
      MenuItems: []
    };
  }

  componentDidMount() {
    this.fetchMenuItems();
    if (window.innerWidth < 750) {
      this.setState({ navClose: { right: "-410px" } });
    }
    if (window.innerWidth < 1199) {
      this.setState({ navClose: { right: "-300px" } });
    }
  }

  openNav() {
    console.log("open");
    this.setState({ navClose: { right: "0px" } });
  }
  closeNav() {
    this.setState({ navClose: { right: "-410px" } });
  }

  onMouseEnterHandler() {
    if (window.innerWidth > 1199) {
      document.querySelector("#main-menu").classList.add("hover-unset");
    }
  }

  handleSubmenu = event => {
    if (event.target.classList.contains("sub-arrow")) return;
    if (event.target.nextElementSibling.classList.contains("opensubmenu")) event.target.nextElementSibling.classList.remove("opensubmenu");
    else {
      document.querySelectorAll(".nav-submenu").forEach(function(value) {
        value.classList.remove("opensubmenu");
      });
      event.target.nextElementSibling.classList.add("opensubmenu");
    }
  };

  handleMegaSubmenu = event => {
    if (event.target.classList.contains("snullub-arrow")) return;

    if (event.target.parentNode.nextElementSibling.classList.contains("opensubmegamenu"))
      event.target.parentNode.nextElementSibling.classList.remove("opensubmegamenu");
    else {
      document.querySelectorAll(".menu-content").forEach(function(value) {
        value.classList.remove("opensubmegamenu");
      });
      event.target.parentNode.nextElementSibling.classList.add("opensubmegamenu");
    }
  };

  fetchMenuItems() {
    // fetch("https://pluspointapi.unlockretail.com/api/web/v1/department?access-token=9e74904aa7581d08c1465f8fc842630c", {
    //   method: "GET",
    //   cache: "no-cache"
    // })
    //   .then(response => {
    //     return response.json();
    //   })
    //   .then(myJson => {
    //     var departmentList = myJson.data.slice(0, 3);
    //     this.setState({
    //       MenuItems: departmentList
    //     });
    //   });
    this.setState({ MenuItems: departments_json.data.slice(0, 3) });
  }

  render() {
    const { language } = this.props;
    const { MenuItems } = this.state;
    console.log("Menu items", MenuItems);
    return (
      <div>
        <div className="main-navbar">
          <div id="mainnav">
            <div className="toggle-nav" onClick={this.openNav.bind(this)}>
              <i className="fa fa-bars sidebar-bar"></i>
            </div>
            <ul className="nav-menu" style={this.state.navClose}>
              <li className="back-btn" onClick={this.closeNav.bind(this)}>
                <div className="mobile-back text-right">
                  <span>Back</span>
                  <i className="fa fa-angle-right pl-2" aria-hidden="true"></i>
                </div>
              </li>
              {MenuItems.map((Menu, index) => (
                <li key={index}>
                  <Link
                    to={`${process.env.PUBLIC_URL}/products/${Menu.department_slug}`}
                    className="nav-link"
                    onClick={e => this.handleSubmenu(e)}
                  >
                    {language === "en" ? Menu.department_name : Menu.department_name_bd}
                    <span className="sub-arrow"></span>
                  </Link>
                  <ul className="nav-submenu">
                    {Menu.subdepartment.slice(0, 3).map((subdepartment, index) => (
                      <li key={index}>
                        <Link to={`${process.env.PUBLIC_URL}/products/${subdepartment.subdepartment_slug}`}>
                          {language === "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                        </Link>
                      </li>
                    ))}
                  </ul>
                </li>
              ))}
              {/* <li className="mega-menu">
                <Link
                  to={`${process.env.PUBLIC_URL}/collection/category/asdf`}
                  className="dropdown"
                  onClick={e => this.handleSubmenu(e)}
                >
                  {language == "en" ? Menu.department_name : Menu.department_name_bd} <span className="sub-arrow"></span>
                </Link>
                <div className="mega-menu-container">
                  <div className="container">
                    <div className="row">
                      {Menu.subdepartment.slice(0, 3).map((subdepartment, index) => (
                        <div className="col mega-box">
                          <div className="link-section">
                            <div className="menu-title">
                              <Link to={`${process.env.PUBLIC_URL}/collection/category/asdf`}>
                                <h5 onClick={e => this.handleMegaSubmenu(e)}>
                                  {language == "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                                  <span className="sub-arrow"></span>
                                </h5>
                              </Link>
                            </div>
                            <div className="menu-content">
                              <ul>
                                <li>
                                  <Link to={`${process.env.PUBLIC_URL}/features/element-title`}>element_title</Link>
                                </li>
                                <li>
                                  <Link to={`${process.env.PUBLIC_URL}/features/element-banner`}>collection_banner</Link>
                                </li>
                                <li>
                                  <Link to={`${process.env.PUBLIC_URL}/features/element-slider`}>home_slider</Link>
                                </li>
                                <li>
                                  <Link to={`${process.env.PUBLIC_URL}/features/element-category`}>category</Link>
                                </li>
                                <li>
                                  <Link to={`${process.env.PUBLIC_URL}/features/element-service`}>service</Link>
                                </li>
                                
                              </ul>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </li> */}

              {/* <li>
                <Link to="#" className="nav-link" onClick={e => this.handleSubmenu(e)}>
                  {translate("pages")}
                  <span className="sub-arrow"></span>
                </Link>
                <ul className="nav-submenu">
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/about-us`}>{translate("about_us")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/404`}>404</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/lookbook`}>{translate("lookbook")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/login`}>{translate("login")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/register`}>{translate("register")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/search`}>{translate("search")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/collection`}>{translate("collection")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/forget-password`}>{translate("forget_password")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/contact`}>{translate("contact")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/dashboard`}>{translate("dashboard")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/pages/faq`}>{translate("FAQ")}</Link>
                  </li>
                </ul>
              </li>
              <li>
                <Link to="#" className="nav-link" onClick={e => this.handleSubmenu(e)}>
                  {translate("blog")}
                  <span className="sub-arrow"></span>
                </Link>
                <ul className="nav-submenu">
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/blog/blog-page`}>{translate("blog_left_sidebar")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/blog/right-sidebar`}>{translate("blog_right_sidebar")}</Link>
                  </li>
                  <li>
                    <Link to={`${process.env.PUBLIC_URL}/blog/details`}>{translate("blog_detail")}</Link>
                  </li>
                </ul>
              </li> */}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps)(NavBar);
