import React from 'react';
import {Link} from 'react-router-dom'
import { connect } from "react-redux";


const CartHeader  =({item, total, symbol, removeFromCart, language}) => (
            <li >
                <div className="media">
                    <Link to={`${process.env.PUBLIC_URL}/product/${item.id}`}><img alt="" className="mr-3" src={`${item.pictures[0]}`} /></Link>
                    <div className="media-body">
                        <Link to={`${process.env.PUBLIC_URL}/product/${item.id}`}><h4>{language === "en" ? item.name : item.name_bd}</h4></Link>
                        <h4><span>{item.qty} x {symbol} {(item.salePrice-item.salePrice*(item.discount/100))}</span></h4>
                    </div>
                </div>
                {/*<span>{cart}</span>*/}
                <div className="close-circle">
                    <button onClick={removeFromCart}><i className="fa fa-times" aria-hidden="true"></i></button>
                </div>
            </li>
        )

const mapStateToProps = state => ({
language: state.Intl.locale,
});

export default connect(mapStateToProps, {})(CartHeader);
