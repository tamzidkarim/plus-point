import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

// Import custom components
import store from "../../../store";
import NavBar from "./common/navbar";
import CartContainer from "./../../../containers/CartContainer";
import LogoImage from "./common/logo";
import { changeCurrency, changeUserType } from "../../../actions";
import { connect } from "react-redux";

class HeaderTwo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: "",
      username: "",
      isLoading: false,
      MenuItems: []
    };
  }

  /*=====================
           Pre loader
           ==========================*/
  componentDidMount() {
    setTimeout(function() {
      document.querySelector(".loader-wrapper").style = "display: none";
    }, 300);
  }

  componentWillMount() {
    window.addEventListener("scroll", this.handleScroll);
    const tokenVar = localStorage.getItem("token");
    this.setState({
      token: tokenVar
    });

    const Username = localStorage.getItem("username");
    this.setState({
      username: Username
    });
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    let number = window.pageXOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (number >= 300) {
      if (window.innerWidth < 576) {
        document.getElementById("sticky").classList.remove("fixed");
      } else document.getElementById("sticky").classList.add("fixed");
    } else {
      document.getElementById("sticky").classList.remove("fixed");
    }
  };

  Logout = event => {
    this.props.changeUserType("Normal");
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    window.location.href = "/login";
  };

  changeLanguage(lang) {
    store.dispatch(IntlActions.setLocale(lang));
  }

  openNav() {
    var openmyslide = document.getElementById("mySidenav");
    if (openmyslide) {
      openmyslide.classList.add("open-side");
    }
  }
  openSearch() {
    document.getElementById("search-overlay").style.display = "block";
  }

  closeSearch() {
    document.getElementById("search-overlay").style.display = "none";
  }

  load = () => {
    this.setState({ isLoading: false });
    fetch().then(() => {
      // deal with data fetched
      this.setState({ isLoading: false });
    });
  };

  render() {
    const { translate, language } = this.props;
    const { token, username } = this.state; //User Login Token
    const { userType } = this.props;
    return (
      <div>
        <header id="sticky" className="sticky" style={{ borderBottom: "1px solid #ddd" }}>
          {this.state.isLoading ? <Pace color="#27ae60" /> : null}
          <div className="mobile-fix-option"></div>
          {/*Top Header Component*/}
          <div style={{ borderBottom: "1px solid #ddd" }}>
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div className="main-menu border-top-0">
                    <div className="brand-logo layout2-logo">
                      <LogoImage logo={this.props.logoName} />
                    </div>
                    <div className="menu-right pull-right">
                      <div>
                        <div className="icon-nav">
                          <ul>
                            {token ? (
                              <li>
                                <div className="dropdown">
                                  <button
                                    style={{ background: "none" }}
                                    className="btn dropdown-toggle bdt-button-custom"
                                    type="button"
                                    id="dropdownMenuButton"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/user-icon.png`} alt="user" />{" "}
                                    <span className="top-user-name">{username}</span>
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/button-drop-arrow.png`} />
                                  </button>
                                  {userType != "Guest" ? (
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <ul className="onhover-show-div">
                                        <li>
                                          <Link to={`${process.env.PUBLIC_URL}/dashboard`} data-lng="en">
                                            {language == "en" ? "Dashboard" : "ড্যাশবোর্ড"}
                                          </Link>
                                        </li>
                                        <li>
                                          <Link to={`${process.env.PUBLIC_URL}/my-orders`} data-lng="en">
                                            {language == "en" ? "Your Orders" : "আপনার অর্ডার"}
                                          </Link>
                                        </li>
                                        <li>
                                          <button data-lng="en" onClick={this.Logout}>
                                            {language == "en" ? "LogOut" : "লগআউট"}
                                          </button>
                                        </li>
                                      </ul>
                                    </div>
                                  ) : (
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <ul className="onhover-show-div">
                                        <li>
                                          <button data-lng="en" onClick={this.Logout}>
                                            {language == "en" ? "LogOut" : "লগআউট"}
                                          </button>
                                        </li>
                                      </ul>
                                    </div>
                                  )}
                                </div>
                              </li>
                            ) : (
                              <li>
                                <Link to={`${process.env.PUBLIC_URL}/login`} data-lng="en">
                                  <img src={`${process.env.PUBLIC_URL}/assets/images/user-icon.png`} alt="user" />{" "}
                                  {language == "en" ? "Sign In" : "সাইন ইন করুন"}
                                </Link>
                              </li>
                            )}

                            {/*Header Cart Component */}

                            <li className="mobile-wishlist">
                              <Link to={`${process.env.PUBLIC_URL}/wishlist`}>
                                <div>
                                  <img src={`${process.env.PUBLIC_URL}/assets/images/whish-list-icon.png`} className="img-fluid" alt="" />
                                  <i></i>
                                </div>
                              </Link>
                            </li>
                            <li>
                              <CartContainer />
                            </li>
                            <li>
                              <div className="dropdown">
                                <button
                                  style={{ background: "none" }}
                                  className="btn dropdown-toggle bdt-button-custom"
                                  type="button"
                                  id="dropdownMenuButton"
                                  data-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                  {language == "en" ? "BDT (TK)" : "বিডিটি (টাকা)"}{" "}
                                  <img src={`${process.env.PUBLIC_URL}/assets/images/button-drop-arrow.png`} />
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a className="dropdown-item" href="#">
                                    {language == "en" ? "US Dollar" : "আমেরিকান ডলার"}
                                  </a>
                                </div>
                              </div>
                            </li>
                            {/*language setting area*/}
                            <li className="mobile-setting">
                              <div className="dropdown">
                                <button
                                  style={{ background: "none" }}
                                  className="btn dropdown-toggle bdt-button-custom"
                                  type="button"
                                  id="dropdownMenuButton"
                                  data-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                  {language == "en" ? "English" : "বাংলা"}{" "}
                                  <img src={`${process.env.PUBLIC_URL}/assets/images/button-drop-arrow.png`} />
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <ul>
                                    <li>
                                      <button href={null} onClick={() => this.changeLanguage("en")}>
                                        {language == "en" ? "ইংরেজি" : "English"}
                                      </button>{" "}
                                    </li>
                                    <li>
                                      <button href={null} onClick={() => this.changeLanguage("bn")}>
                                        {language == "en" ? "বাংলা" : "Bangla"}
                                      </button>{" "}
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <nav className="navbar navbar-expand-sm navbar-expand-md navbar-light  static-top">
            <div className="container">
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarCollapse"
                aria-controls="navbarCollapse"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarCollapse">
                <ul className="navbar-nav mr-auto">
                  {MenuItems.map((Menu, index) => (
                    <li className="nav-item dropdown">
                      <a
                        className="nav-link dropdown-toggle"
                        href={`${process.env.PUBLIC_URL}/collection/category/${Menu.department_slug}`}
                        id="navbarDropdown"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        {language == "en" ? Menu.department_name : Menu.department_name_bd}{" "}
                        <img src={`${process.env.PUBLIC_URL}/assets/images/button-drop-arrow.png`} />
                      </a>
                      <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                        {Menu.subdepartment.slice(0, 3).map((subdepartment, index) => (
                          <a
                            className="dropdown-item"
                            href={`${process.env.PUBLIC_URL}/collection/category/${subdepartment.subdepartment_slug}`}
                          >
                            {" "}
                            {language == "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                          </a>
                        ))}
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
              <form className="search">
                <div className="search__wrapper">
                  <input type="text" name="" placeholder="Search for..." className="search__field" />
                  <button type="submit" className="fa fa-search search__icon"></button>
                </div>
              </form>
              <div>
                <img
                  src={`${process.env.PUBLIC_URL}/assets/images/icon/search.png`}
                  onClick={this.openSearch}
                  className="img-fluid"
                  alt=""
                />
                <i className="fa fa-search main-search" onClick={this.openSearch}></i>
              </div>
            </div>
          </nav> */}
          <div className="container">
            <div className="row">
              <div className="col-lg-11 col-sm-10 col-md-10">
                <div className="main-nav-center">
                  <NavBar />
                </div>
              </div>
              <div className="col-lg-1 col-sm-2 col-md-2" style={{ margin: "auto 0", textAlign: "right" }}>
                <i className="fa fa-search main-search" onClick={this.openSearch}></i>
              </div>
            </div>
          </div>
          <div style={{ borderBottom: "1px solid #ddd" }}></div>
        </header>

        <div id="search-overlay" className="search-overlay">
          <div>
            <span className="closebtn" onClick={this.closeSearch} title="Close Overlay">
              ×
            </span>
            <div className="overlay-content">
              <div className="container">
                <div className="row">
                  <div className="col-xl-12">
                    <form>
                      <div className="form-group">
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Search a Product" />
                      </div>
                      <button type="submit" className="btn btn-primary">
                        <i className="fa fa-search"></i>
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale,
  userType: state.userType.userType
});

export default connect(mapStateToProps, { changeCurrency, changeUserType })(HeaderTwo);
