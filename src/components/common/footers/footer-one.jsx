import React, { Component } from "react";
import { Link } from "react-router-dom";

import { SlideUpDown } from "../../../services/script";
import LogoImage from "../headers/common/logo";
import { changeCurrency } from "../../../actions";
import { connect } from "react-redux";
import Axios from "axios";
import store from "../../../store";

class FooterOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      MenuItems: []
    };
  }
  componentDidMount() {
    var contentwidth = window.innerWidth;
    if (contentwidth < 750) {
      SlideUpDown("footer-title");
    } else {
      var elems = document.querySelectorAll(".footer-title");
      [].forEach.call(elems, function(elemt) {
        let el = elemt.nextElementSibling;
        el.style = "display: block";
      });
    }
    this.fetchMenuItems();
  }

  fetchMenuItems() {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/department?access-token=9e74904aa7581d08c1465f8fc842630c", {
      method: "GET"
    })
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        var departmentList = myJson.data.slice(0, 3);
        this.setState({
          MenuItems: departmentList
        });
      });
  }

  render() {
    const { language } = this.props;

    var { MenuItems } = this.state;

    return (
      <footer className="page-footer font-small mdb-color lighten-3 pt-4">
        <div className="container">
          <div className="row">
            <div className="col-md-7">
              <div className="subscribe-text mb-15">
                <h2>{language == "en" ? "JOIN OUR NEWSLETTER" : "আমাদের সংবাদ সংকলনে যোগদান করুন"}</h2>
                <p>{language == "en" ? "Sign Up for Plus Point updates to receive information about New Arrival, Futures and Specials" : "নতুন আগমন, ফিউচার এবং বিশেষ সম্পর্কিত তথ্য পেতে প্লাস পয়েন্ট আপডেটের জন্য সাইন আপ করুন"}</p>
              </div>
            </div>
            <div className="col-md-5 text-right">
              <div className="subscribe-wrapper subscribe2-wrapper">
                <div className="subscribe-form">
                  <form action="#">
                    <input placeholder={language == "en" ? "Enter our email address" : "ইমেইল লিখুন"} className="subscribe-input" type="email" required/>
                    <button className="subscribe-button">
                       {language == "en" ? "subscribe" : "সাবস্ক্রাইব"} <i className="fas fa-long-arrow-alt-right"></i>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br></br>
        <br></br>
        <div className="container text-center text-md-left">
          <div className="row">
            <div className="col">
              <h5 className="font-weight-bold text-uppercase mb-4">{language == "en" ? "SHOP" : "দোকান"}</h5>
              <ul className="list-group">
                {MenuItems.map((Menu, index) => (
                  <li key={index}>
                    {Menu.subdepartment.slice(0, 3).map((subdepartment, index) => (
                      <a
                        style={{ display: "block" }}
                        href={`${process.env.PUBLIC_URL}/collection/subcategory/${subdepartment.subdepartment_slug}`}
                        key={index}
                      >
                        {/* {subdepartment.subdepartment_name} */}
                        {language == "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                      </a>
                    ))}
                  </li>
                ))}
              </ul>
            </div>
            <hr className="clearfix w-100 d-md-none" />
            <div className="col">
              <h5 className="font-weight-bold text-uppercase mb-4">{language == "en" ? "Information" : "তথ্য"}</h5>

              <ul className="list-group">
                <li>
                  <Link to="/pages/about-us">{language == "en" ? "About Us" : "আমাদের সম্পর্কে"}</Link>
                </li>
                <li>
                  <Link to="/pages/customer-service">{language == "en" ? "Customer Service" : "গ্রাহক সেবা"}</Link>
                </li>
                <li>
                  <Link to="/blog/right-sidebar">{language == "en" ? "Blog" : "ব্লগ"}</Link>
                </li>
                <li>
                  <Link to="/pages/contact">{language == "en" ? "Contact Us" : "যোগাযোগ করুন "}</Link>
                </li>
                <li>
                  <Link to="/pages/terms-conditions">{language == "en" ? "Terms & Conditions" : "শর্তাবলী"}</Link>
                </li>
              </ul>
            </div>
            <hr className="clearfix w-100 d-md-none" />
            <div className="col">
              <h5 className="font-weight-bold text-uppercase mb-4">{language == "en" ? "Order" : "আদেশ"}</h5>

              <ul className="list-group">
                <li>
                  <Link to="/dashboard">{language == "en" ? "My Account" : "আমার অ্যাকাউন্ট"}</Link>
                </li>
                <li>
                  <Link to="/cart">{language == "en" ? "View Bag" : "ব্যাগ দেখুন"}</Link>
                </li>
                <li>
                  <Link to="/pages/privacy">{language == "en" ? "Return Policy" : "প্রত্যাবর্তন নীতিমালা"}</Link>
                </li>
              </ul>
            </div>
            <hr className="clearfix w-100 d-md-none" />
            <hr className="clearfix w-100 d-md-none" />
            <div className="col">
              <h5 className="font-weight-bold text-uppercase mb-4">{language == "en" ? "Here To Help" : "সাহায্য পেতে"}</h5>
              <p>
                {language == "en"
                  ? "Have a question? You may find an answer in our FAQs."
                  : "আমাদের FAQ গুলিতে আপনি একটি উত্তর পেতে পারেন।"}{" "}
              </p>
              <p> {language == "en" ? "But you can also contact us." : "তবে আপনি আমাদের সাথেও যোগাযোগ করতে পারেন।"}</p>
              <br />
              <h6 className="font-weight-bold text-uppercase mb-4">{language == "en" ? "Customer Service" : "গ্রাহক সেবা"}</h6>
              <p>{language == "en" ? "Call us: +8801711111111" : "আমাদের কল করুন: +৮৮০১৭১১১১১১"}</p>
            </div>
            <hr className="clearfix w-100 d-md-none" />
            <div className="col">
              <h5 className="font-weight-bold text-uppercase mb-4">{language == "en" ? "Follow Us" : "আমাদের অনুসরণ করুন"}</h5>

              <p>
                <i className="fa fa-facebook-f"></i>
                <a href="https://www.facebook.com/pluspointbd/"> {language == "en" ? "Facebook" : "ফেসবুক"} </a>
              </p>
              <br></br>

              <p>
                <i className="fa fa-twitter"></i>
                <a href="https://www.facebook.com/pluspointbd/"> {language == "en" ? "Twitter" : "টুইটার"} </a>
              </p>
              <br></br>

              <p>
                <i className="fa fa-instagram"></i>
                <a href="https://www.facebook.com/pluspointbd/"> {language == "en" ? "Instagram" : "ইনস্টাগ্রাম"} </a>
              </p>
              <br></br>
              <p>
                <i className="fa fa-pinterest"></i>
                <a href="https://www.facebook.com/pluspointbd/"> {language == "en" ? "Pinterest" : "পিন্টারেস্ট"} </a>
              </p>
            </div>
          </div>
        </div>

        <div className="footer-brand">
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <img src={`${process.env.PUBLIC_URL}/assets/images/icon/logo.png`} alt="user" />
              </div>
              <div className="col-md-5">
              <p>{language == "en" ? "Sign Up for Plus Point updates to receive information about New Arrival, Futures and Specials" : "নতুন  তথ্য পেতে প্লাস পয়েন্ট আপডেটের জন্য সাইন আপ করুন"}</p>
              </div>
              <div className="col-md-5">
                <img src={`${process.env.PUBLIC_URL}/assets/images/payment-getwaye.png`} alt="user" />
              </div>
            </div>
          </div>
        </div>

        <div className="footer-copyright text-center py-3">
        {language == "en" ? "© 2019 Copyright:" : "© ২০১৯ কপিরাইটস"}
          <span> Plus Point</span>
        </div>
      </footer>
    );
  }
}
const mapStateToProps = state => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps)(FooterOne);
