import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Breadcrumb from "../common/breadcrumb";
import { getCartTotal } from "../../services";
import { removeFromCart, incrementQty, decrementQty } from "../../actions";

class cartComponent extends Component {
  render() {
    const { cartItems, symbol, total,language } = this.props;
    return (
      <div>
        {/*SEO Support*/}
        <Helmet>
          <title>Pluspoint | Cart List Page</title>
          <meta
            name="description"
            content="Multikart – Multipurpose eCommerce React Template is a multi-use React template. It is designed to go well with multi-purpose websites. Multikart Bootstrap 4 Template will help you run multiple businesses."
          />
        </Helmet>
        {/*SEO Support End */}

        <Breadcrumb title={"Cart Page"} />

        {cartItems.length > 0 ? (
          <section className="cart-section section-b-space">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <table className="table cart-table table-responsive-xs">
                    <thead>
                      <tr className="table-head">
                        <th scope="col">{language === "en" ? "image" : "ইমেজ"}</th>
                        <th scope="col">{language === "en" ? "product name" : "পণ্যের নাম"}</th>
                        <th scope="col">{language === "en" ? "price" : "মূল্য"}</th>
                        <th scope="col">{language === "en" ? "quantity" : "পরিমাণ"}</th>
                        <th scope="col">{language === "en" ? "action" : "প্রক্রিয়া"}</th>
                        <th scope="col">{language === "en" ? "total" : "মোট"}</th>
                      </tr>
                    </thead>
                    {cartItems.map((item, index) => {
                      return (
                        <tbody key={index}>
                          <tr>
                            <td>
                              <Link to={`${process.env.PUBLIC_URL}/product/${item.id}`}>
                                <img src={item.variants ? item.variants[0].images : item.pictures[0]} alt="" />
                              </Link>
                            </td>
                            <td>
                              <Link to={`${process.env.PUBLIC_URL}/product/${item.id}`}>{item.name}</Link>
                              <div className="mobile-cart-content row">
                                <div className="col-xs-3">
                                  <div className="qty-box">
                                    <div className="input-group">
                                      <input type="text" name="quantity" className="form-control input-number" defaultValue={item.qty} />
                                    </div>
                                  </div>
                                </div>
                                <div className="col-xs-3">
                                  <h2 className="td-color">
                                    {symbol}
                                    {item.salePrice - (item.salePrice * item.discount) / 100}
                                  </h2>
                                </div>
                                <div className="col-xs-3">
                                  <h2 className="td-color">
                                    <a href="!#" className="icon" onClick={() => this.props.removeFromCart(item)}>
                                      <i className="icon-close"></i>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </td>
                            <td>
                              <h2>
                                {symbol}
                                {item.salePrice - (item.salePrice * item.discount) / 100}
                              </h2>
                            </td>
                            <td>
                              <div className="qty-box">
                                <div className="input-group">
                                  <span className="input-group-prepend">
                                    {item.qty > 1 ? (
                                      <button
                                        type="button"
                                        className="btn quantity-left-minus"
                                        onClick={() => this.props.decrementQty(item.id)}
                                        data-type="minus"
                                        data-field=""
                                      >
                                        <i className="fa fa-minus"></i>
                                      </button>
                                    ) : (
                                      <button type="button" className="btn quantity-left-minus disabled" data-type="minus" data-field="">
                                        <i className="fa fa-minus"></i>
                                      </button>
                                    )}
                                  </span>
                                  <input
                                    type="text"
                                    name="quantity"
                                    value={item.qty}
                                    readOnly={true}
                                    className="form-control input-number"
                                  />

                                  <span className="input-group-prepend">
                                    <button
                                      className="btn quantity-right-plus"
                                      onClick={() => this.props.incrementQty(item.id)}
                                      data-type="plus"
                                      disabled={item.qty >= item.stock ? true : false}
                                    >
                                      <i className="fa fa-plus"></i>
                                    </button>
                                  </span>
                                </div>
                              </div>
                              {item.qty >= item.stock ? "out of Stock" : ""}
                            </td>
                            <td>
                              <button className="icon" onClick={() => this.props.removeFromCart(item)}>
                                <i style={{ fontSize: "20px" }} className="fa fa-trash"></i>
                              </button>
                            </td>
                            <td>
                              <h2 className="td-color">
                                {symbol}
                                {item.sum}
                              </h2>
                            </td>
                          </tr>
                        </tbody>
                      );
                    })}
                  </table>
                  <table className="table cart-table table-responsive-md">
                    <tfoot>
                      <tr>
                        <td>{language === "en" ? "total price :" : "মোট দাম :"}</td>
                        <td>
                          <h2>
                            {symbol} {total}{" "}
                          </h2>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <div className="row cart-buttons">
                <div className="col-6">
                  <Link to={`${process.env.PUBLIC_URL}/left-sidebar/collection`} className="btn btn-solid-cart">
                    {language === "en" ? "continue shopping" : "কেনাকাটা চালিয়ে যান"}
                  </Link>
                </div>
                <div className="col-6">
                  <Link to={`${process.env.PUBLIC_URL}/login`} className="btn btn-solid-cart">
                    {language === "en" ? "check out" : "চেক আউট"}
                  </Link>
                </div>
              </div>
            </div>
          </section>
        ) : (
          <section className="cart-section section-b-space">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div>
                    <div className="col-sm-12 empty-cart-cls text-center">
                      <img src={`${process.env.PUBLIC_URL}/assets/images/icon-empty-cart.png`} className="img-fluid mb-4" alt="" />
                      <h3>
                        <strong>{language === "en" ? "Your Cart is Empty" : "আপনার কার্ট খালি"}</strong>
                      </h3>
                      <h4>{language === "en" ? "Explore more shortlist some items." : "কিছু শর্টলিস্ট কিছু আইটেম এক্সপ্লোর করুন।"}</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  language: state.Intl.locale,
  cartItems: state.cartList.cart,
  symbol: state.data.symbol,
  total: getCartTotal(state.cartList.cart)
});

export default connect(mapStateToProps, { removeFromCart, incrementQty, decrementQty })(cartComponent);
