import React, { Component } from "react";
import { Helmet } from "react-helmet";
import "../../common/index.scss";
import Slider from "react-slick";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import MessengerCustomerChat from "react-messenger-customer-chat";
// Import custom components
import Trending from "./trending";
import HeaderTwo from "../../common/headers/header-two";
import FooterOne from "../../common/footers/footer-one";
import ThemeSettings from "../../common/theme-settings";
import SpecialProducts from "./products";
import Followus from "./Followus";
import Findus from "./Find_Us";
import _ from "lodash";

class Vegetables extends Component {
  constructor(props) {
    super(props);
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.state = {
      products: [],
      sliderImages: null
    };
  }

  play() {
    this.slider.slickPlay();
  }
  pause() {
    this.slider.slickPause();
  }

  componentDidMount() {
    document.getElementById("color").setAttribute("href", `#`);
    // this.getAllProducts();
    this.getSliderImages();
  }

  getSliderImages() {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/sliders?access-token=9e74904aa7581d08c1465f8fc842630c", {
      method: "GET",
      cache: "no-cache"
    })
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        this.setState({
          sliderImages: myJson.data
        });
      });
  }

  render() {
    let { sliderImages } = this.state;
    let { products, language } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 8000
    };
    return (
      <div>
        <Helmet>
          <title>Welcome | Pluspoint</title>
        </Helmet>
        <HeaderTwo logoName={"logo.png"} />
        <section className="p-0">
          <Slider ref={slider => (this.slider = slider)} {...settings} className="slide-1 home-slider">
            {sliderImages &&
              _.map(sliderImages, (slide, index) => (
                <div key={index}>
                  <div style={{ backgroundImage: `url("${slide.image}")` }} className="home text-center">
                    <div className="container">
                      <div className="row">
                        <div className="col">
                          <div className="slider-contain">
                            <div>
                              <h2>{slide.secondTitle}</h2>
                              <h4>{slide.shortDesc}</h4>
                              <Link to={`${process.env.PUBLIC_URL}/products/all`} className="btn btn-solid">
                                {language === "en" ? " shop now" : "কিনুন"}
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
          </Slider>
        </section>
        <SpecialProducts product={products} />

        {/*Blog Section*/}
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="new-arrivals">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/new-arrivals.png`}
                  alt="new arrivals"
                />
                <div className="new-arrivals-info">
                  <h3>New Arrivals</h3>
                  <h2>TOUCH OF COLOR</h2>
                  <p>Essential styles come alive in bright colors</p>
                  <a href="!#" className="discover-button">
                    DISCOVER MORE
                  </a>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="new-arrivals">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/discover.png`}
                  alt="new arrivals"
                />
                <div className="new-arrivals-info">
                  <h3>Discover Item All</h3>
                  <h2>This season's bomber jackets</h2>
                  <a href="!#" className="discover-button">
                    DISCOVER MORE
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*Blog Section End*/}

        {/*product section Start*/}
        <Trending type={"vegetable"} product={products} />
        {/*product section End*/}
        <Followus />
        <Findus />
        <ThemeSettings />
        <FooterOne logoName={"logo.png"} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.data.products,
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps)(Vegetables);
