/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";

// let BASE_URL = "http://unlockgiftcards.com";
// const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-new-arival`;

class Followus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      facebookItems: []
    };
  }

  componentDidMount() {
    this.getContent();
  }

  getContent() {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/product?access-token=9e74904aa7581d08c1465f8fc842630c")
      .then(res => res.json())
      .then(json => {
        this.setState({
          items: json.data
        });
      });
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/facebook-api?access-token=9e74904aa7581d08c1465f8fc842630c")
      .then(res => res.json())
      .then(json => {
        this.setState({
          facebookItems: json.data
        });
      });
  }

  render() {
    var { facebookItems } = this.state;
    const { language } = this.props;
    return (
      <div>
        {/* Facebook  */}
        {/* <section className="section-b-space addtocart_count ratio_square"> */}
        <div className="container">
          <div className="title4">
            <h4 className="text-xl-center">
              <span>{language == "en" ? "Plus Point Follow Us on Facebook" : "ফেসবুকে আমাদের অনুসরণ করুন"}</span>
            </h4>
          </div>
          <br />
          <div className="container facebook">
            <div className="row">
              {facebookItems.slice(0, 6).map(item => {
                return (
                  <a href={item.permalink_url} className="col-lg-2 col-md-2 col-sm-4">
                    <div className="category-item">
                      <div className="content">
                        <div className="wrap">
                          <img height="198" width="198" className="img" src={item.full_picture} alt="Generic placeholder image" />
                        </div>
                      </div>
                    </div>
                  </a>
                );
              })}
            </div>
          </div>
        </div>
        <br />
        {/* FACEBOOK end  */}
        {/* </section> */}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(Followus);
