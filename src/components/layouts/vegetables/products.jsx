import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { connect } from "react-redux";

import { getBestSeller, getMensWear, getWomensWear } from "../../../services/index";
import { addToCart, addToWishlist, addToCompare } from "../../../actions/index";
import ProductItem from "./product-item";
import departments_json from "../../../constants/departments.json";

class SpecialProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departmentList: [],
      subDepartment: [],
      products: []
    };
  }

  componentDidMount() {
    this.fetchDepartment();
  }

  loadProductPreviewImages(product) {
    if (product.imageSize.length === 0) {
      return <img src={`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />;
    }
    product.imageSize.map(image => {
      return <img src={`${process.env.PUBLIC_URL}/assets/images/item-01.png`} alt="" />;
    });
  }

  fetchDepartment() {
    // fetch("https://pluspointapi.unlockretail.com/api/web/v1/department?access-token=9e74904aa7581d08c1465f8fc842630c", {
    //   method: "GET"
    // })
    //   .then(response => {
    //     return response.json();
    //   })
    //   .then(myJson => {
    //     var departmentList = myJson.data.slice(0, 3);
    //     this.setState({
    //       departmentList: departmentList
    //     });
    //   });
    this.setState({ departmentList: departments_json.data.slice(0,3) });
  }

  render() {
    var { departmentList } = this.state;
    let { newProducts, featuredProducts, specialProducts, language } = this.props;
    const { symbol, addToCart, addToWishlist, addToCompare } = this.props;

    return (
      <div>
        {/* main category start  */}
        <div className="container marketing">
          <div className="row">
            {departmentList.map((department, index) => (
              <div className="col-lg-4 col-md-4 col-sm-4">
                <a href={`${process.env.PUBLIC_URL}/products/${department.department_slug}`}>
                  <div className="category-item">
                    <div className="content">
                      <div className="wrap">
                        <img className="img-fluid" src={`${department.department_image}`} alt="Generic placeholder image" />
                      </div>
                      {/* <div className="content2-overlay"></div> */}
                      <div className="content-overlay"></div>
                      <div className="content-details fadeIn-bottom">
                        <h3 className="content-title">{language === "en" ? department.department_name : department.department_name_bd}</h3>
                        <ul className="list-group">
                          {department.subdepartment.slice(0, 3).map((subdepartment, index) => (
                            <li>
                              <a
                                className="content-text"
                                href={`${process.env.PUBLIC_URL}/products/${department.department_slug}/${subdepartment.subdepartment_slug}`}
                                key={index}
                              >
                                {language === "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                              </a>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <a
                      className="plus-point-cat-btn"
                      href={`${process.env.PUBLIC_URL}/products/${department.department_slug}`}
                      role="button"
                    >
                      {language === "en" ? department.department_name : department.department_name_bd}
                    </a>
                  </div>
                </a>
              </div>
            ))}
          </div>
        </div>
        {/* main category end  */}
        <br></br>
        <br></br>

        {/* tab contents */}

        <section className="section-b-space p-t-0">
          <div className="container">
            <Tabs className="theme-tab">
              <TabList className="tabs tab-title">
                <Tab> {language === "en" ? "New Products" : "নতুন পণ্য"}</Tab>
                <Tab>{language === "en" ? "Special Products" : "বিশেষ পণ্য"}</Tab>
                <Tab>{language === "en" ? "Featured Products" : "বৈশিষ্ট্যযুক্ত পণ্য"}</Tab>
              </TabList>

              <TabPanel>
                <div className="row">
                  {newProducts.map((product, index) => (
                    <ProductItem
                      product={product}
                      symbol={symbol}
                      onAddToCompareClicked={() => addToCompare(product)}
                      onAddToWishlistClicked={() => addToWishlist(product)}
                      key={index}
                    />
                  ))}
                </div>
              </TabPanel>
              <TabPanel>
                <div className="row">
                  {specialProducts.map((product, index) => (
                    <ProductItem
                      product={product}
                      symbol={symbol}
                      onAddToCompareClicked={() => addToCompare(product)}
                      onAddToWishlistClicked={() => addToWishlist(product)}
                      key={index}
                    />
                  ))}
                </div>
              </TabPanel>
              <TabPanel>
                <div className="row">
                  {featuredProducts.map((product, index) => (
                    <ProductItem
                      product={product}
                      symbol={symbol}
                      onAddToCompareClicked={() => addToCompare(product)}
                      onAddToWishlistClicked={() => addToWishlist(product)}
                      key={index}
                    />
                  ))}
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  newProducts: getBestSeller(state.data.products),
  featuredProducts: getMensWear(state.data.products),
  specialProducts: getWomensWear(state.data.products),
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {
  addToCart,
  addToWishlist,
  addToCompare
})(SpecialProducts);
