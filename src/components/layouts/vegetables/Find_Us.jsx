/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
// let BASE_URL = "http://unlockgiftcards.com";
// const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-new-arival`;

class Find_Us extends Component {
  constructor(props) {
    super(props);
  }

  
  render() {
    const { language } = this.props;
    return (
      <div>
        <div className="container">
          <div className="title4">
            <h4 className="text-xl-center">
              <span>{language == "en" ? "Find Us Near You" : "আপনার কাছাকাছি আমাদের সন্ধান করুন"}</span>
            </h4>
          </div> 
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="new-arrivals">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/mobile-map.png`}
                  alt="new arrivals"
                />
              </div>
            </div>
            <div className="col-md-6" style={{ textAlign: "center", margin: "auto" }}>
              <div className="dropdown show">
                <a
                  className="btn"
                  href="!#"
                  role="button"
                  id="dropdownMenuLink"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style={{ backgroundColor: "#fff", color: "#000", border: "2px solid #000" }}
                >
                  Select District
                </a>
                <div className="btn btn-solid" style={{ padding: "6px", background: "#000 !important", color: "#fff !important", marginLeft:"5px"}} >
                  <Link to={`${process.env.PUBLIC_URL}/findus`} className="view-cart">
                    {language === "en" ? "Get Us" : "আমাদের পেতে"}
                  </Link>
                </div>

                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a className="dropdown-item">
                    Dhaka
                  </a>
                  <a className="dropdown-item">
                    Khulna
                  </a>
                  <a className="dropdown-item">
                    Chattagram
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(Find_Us);

