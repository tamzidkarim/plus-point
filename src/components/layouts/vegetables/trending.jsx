/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getTopCollection, getTrendingCollection } from "../../../services";
import { Product4 } from "../../../services/script";
import { addToCart, addToWishlist, addToCompare, incrementQty, decrementQty, removeFromCart } from "../../../actions";
import ProductItem from "../common/special-product-item";
import Followus from "./Followus";

// let BASE_URL = "http://unlockgiftcards.com";
// const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-new-arival`;

class Trending extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      facebookItems: []
    };
  }

  componentDidMount() {
    this.getContent();
  }

  getContent() {
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/product?access-token=9e74904aa7581d08c1465f8fc842630c")
      .then(res => res.json())
      .then(json => {
        this.setState({
          items: json.data
        });
      });
    fetch("https://pluspointapi.unlockretail.com/api/web/v1/facebook-api?access-token=9e74904aa7581d08c1465f8fc842630c")
      .then(res => res.json())
      .then(json => {
        this.setState({
          facebookItems: json.data
        });
      });
  }

  render() {
    var { items, facebookItems } = this.state;
    const { symbol, addToCart, addToWishlist, addToCompare, incrementQty, decrementQty, removeFromCart, language } = this.props;
    return (
      <div>
        {/*Paragraph*/}
        <section className="section-b-space addtocart_count ratio_square">
          <div className="container">
            <div className="row">
              <div className="col">
                <div className="title4">
                  <h4 className="text-xl-center">
                    <span>{language === "en" ? "Trending Now" : "চলমান"}</span>
                  </h4>

                  <div className="line">
                    <span></span>
                  </div>
                </div>
                <br></br>
                <Slider {...Product4} className="product-4 product-m no-arrow">
                  {items.map((product, index) => (
                    <div key={index}>
                      <ProductItem
                        product={product}
                        symbol={symbol}
                        onAddToCompareClicked={() => addToCompare(product)}
                        onAddToWishlistClicked={() => addToWishlist(product)}
                        onAddToCartClicked={() => addToCart(product, 1)}
                        onIncrementClicked={() => incrementQty(product, 1)}
                        onDecrementClicked={() => decrementQty(product.id)}
                        onRemoveFromCart={() => removeFromCart(product)}
                        key={index}
                      />
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
          </div>
        </section>
        
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  items: getTrendingCollection(ownProps.product, ownProps.type),
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {
  addToCart,
  addToWishlist,
  addToCompare,
  incrementQty,
  decrementQty,
  removeFromCart
})(Trending);
