import React, { Component } from "react";
import { Link } from "react-router-dom";
import Modal from "react-responsive-modal";
import { connect } from "react-redux";
import { addToCart } from "../../../actions";

class SpecialProductItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      openQuantity: false,
      stock: "InStock",
      size: "",
      colour: "",
      quantity: 1,
      image: ""
    };
  }

  onClickHandle(img) {
    this.setState({ image: img });
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  minusQty = () => {
    if (this.state.quantity > 1) {
      this.setState({ stock: "InStock" });
      this.setState({ quantity: this.state.quantity - 1 });
      this.props.onDecrementClicked();
    } else {
      console.log("removefromcart");
      this.setState({ openQuantity: false });
      this.props.onRemoveFromCart();
    }
  };

  plusQty = () => {
    if (this.props.product.stock >= this.state.quantity) {
      this.setState({ quantity: this.state.quantity + 1 });
      this.props.onIncrementClicked();
    } else {
      this.setState({ stock: "Out of Stock !" });
    }
  };
  changeQty = e => {
    this.setState({ quantity: parseInt(e.target.value) });
  };
  updateQty = e => {
    if (this.props.product.stock >= parseInt(e.target.value)) {
      this.setState({ quantity: parseInt(e.target.value) });
      this.props.onAddToCartClicked();
    } else {
      this.setState({ stock: "Out of Stock !" });
    }
  };
  openQuantity = () => {
    this.setState({ openQuantity: true });
    this.props.onAddToCartClicked();
  };

  handleAddToCart = event => {
    let { product } = this.props;
    let { colour, size } = this.state;
    if (product.colors.length == 0 || product.colors[0].length == 0) {
      colour = "default";
    }
    if (colour === null || colour === "") {
      alert("You must choose a colour");
    } else if (size === null || size === "") {
      alert("You must choose a size");
    } else {
      this.props.addToCart(product, 1, colour, size);
    }
  };

  loadProductPreviewImages(product) {
    if (product.imageSize.length == 0) {
      return <img src={`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />;
    } else {
      let items = product.imageSize.slice(0, 2).map((image, index) => {
        if (index == 0) {
          return <img src={`${image.large}`} alt="" className="img-top" key={index} />;
        } else {
          return <img src={`${image.large}`} alt="" key={index} />;
        }
      });
      return items;
    }
  }

  render() {
    const { product, symbol, onAddToCartClicked, onAddToWishlistClicked, onAddToCompareClicked, language } = this.props;
    let RatingStars = [];
    for (var i = 0; i < product.rating; i++) {
      RatingStars.push(<i className="fa fa-star" key={i}></i>);
    }
    return (
      <div>
        <div className="plus-point-tab-item">
          <div className="imageBox" style={{ margin: "0px" }}>
            <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`}>{this.loadProductPreviewImages(product)}</Link>
          </div>
          <p>{language == "en" ? product.subDepartmentName : product.subDepartmentName_bd}</p>
          <div>
            <a className="product-item-title" href="#!">
              {language == "en" ? product.name : product.name_bd}
            </a>
          </div>
          <span className="old-price">
            <del>
              {symbol}
              {product.salePrice}
            </del>
          </span>
          <span className="new-price">
            {symbol}
            {product.salePrice - (product.salePrice * product.discount) / 100}
          </span>
          <ul>
            <li>
              {/* <a href="#!" className="item-add-to-cart-btn" onClick={this.handleAddToCart}>
                ADD TO CART
              </a> */}
              <Link className="item-add-to-cart-btn" to={`${process.env.PUBLIC_URL}/product/${product.id}`}>
                View Details
              </Link>
            </li>
            <li>
              <a href="#!" onClick={onAddToWishlistClicked}>
                <img className="whish-icon" src={`${process.env.PUBLIC_URL}/assets/images/whish-icon.png`} alt="" />
              </a>
            </li>
          </ul>
          <span>{RatingStars}</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, { addToCart })(SpecialProductItem);
