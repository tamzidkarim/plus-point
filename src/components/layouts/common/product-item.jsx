import React, { Component } from "react";
import { Link } from "react-router-dom";
import Modal from "react-responsive-modal";
import connect from "react-redux/lib/connect/connect";

class ProductItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      stock: "InStock",
      quantity: 1,
      image: ""
    };
  }

  onClickHandle(img) {
    this.setState({ image: img });
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  minusQty = () => {
    if (this.state.quantity > 1) {
      this.setState({ stock: "InStock" });
      this.setState({ quantity: this.state.quantity - 1 });
    }
  };

  plusQty = () => {
    if (this.props.product.stock >= this.state.quantity) {
      this.setState({ quantity: this.state.quantity + 1 });
    } else {
      this.setState({ stock: "Out of Stock !" });
    }
  };
  changeQty = e => {
    this.setState({ quantity: parseInt(e.target.value) });
  };

  render() {
    const { product, symbol, onAddToCartClicked, onAddToWishlistClicked, onAddToCompareClicked, language } = this.props;

    let RatingStars = [];
    for (var i = 0; i < product.rating; i++) {
      RatingStars.push(<i className="fa fa-star" key={i}></i>);
    }
    return (
      <div className="product-box">
        <div className="plus-point-tab-item">
          <div className="img-wrapper">
            <div className="lable-block">
              {product.new == true ? <span className="lable3">{language === "en" ? "new" : "নতুন"}</span> : ""}
              {product.sale == true ? <span className="lable4">{language === "en" ? "on sale" : "বিক্রিতে"}</span> : ""}
            </div>
            <div className="">
              <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`}>
                <img
                  src={`${product.variants ? (this.state.image ? this.state.image : product.variants[0].images) : product.pictures[0]}`}
                  className="img-fluid"
                  alt=""
                />
              </Link>
            </div>
          </div>
          <div className="product-detail">
            <div>
              <p style={{ marginBottom: "5px", marginTop: "10px" }}>
                {language == "en" ? product.subDepartmentName : product.subDepartmentName_bd}
              </p>
              <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`}>
                <h6 className="product-item-title">{language == "en" ? product.name : product.name_bd}</h6>
              </Link>
              <span className="old-price">
                <del>৳{product.price}</del>
              </span>{" "}
              <span className="new-price">৳{product.salePrice}</span>
              <div className="product-right">
                <div>
                  <div className="size-box">
                    <ul>
                      <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                        <button>M</button>
                      </li>
                      <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                        <button>L</button>
                      </li>
                      <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                        <button>XL</button>
                      </li>
                      <li style={{ border: "1px solid #000", borderRadius: "0", width: "30px", height: "30px" }}>
                        <button>L</button>
                      </li>
                    </ul>
                  </div>
                </div>
                <div>
                  <div className="size-box">
                    <ul>
                      <li style={{ background: "blue", width: "30px", height: "30px" }}>
                        <button></button>
                      </li>
                      <li style={{ background: "black", width: "30px", height: "30px" }}>
                        <button></button>
                      </li>
                      <li style={{ background: "red", width: "30px", height: "30px" }}>
                        <button></button>
                      </li>
                      <li style={{ background: "Green", width: "30px", height: "30px" }}>
                        <button></button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <ul>
                <li>
                  <button href="#!" className="item-add-to-cart-btn" onClick={() => onAddToCartClicked(product, this.state.quantity)}>
                    {language === "en" ? "ADD TO CART" : "কার্ট যোগ করুন"}
                  </button>
                </li>
                <li>
                  <a href="#!" onClick={onAddToWishlistClicked}>
                    <img className="whish-icon" src={`${process.env.PUBLIC_URL}/assets/images/whish-icon.png`} alt="" />
                  </a>
                </li>
              </ul>
              <div className="rating">{RatingStars}</div>
            </div>
          </div>
          <Modal open={this.state.open} onClose={this.onCloseModal} center>
            <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div className="modal-content quick-view-modal">
                <div className="modal-body">
                  <div className="row">
                    <div className="col-lg-6  col-xs-12">
                      <div className="quick-view-img">
                        <img
                          src={`${
                            product.variants ? (this.state.image ? this.state.image : product.variants[0].images) : product.pictures[0]
                          }`}
                          alt=""
                          className="img-fluid"
                        />
                      </div>
                    </div>
                    <div className="col-lg-6 rtl-text">
                      <div className="product-right">
                        <h2> {product.name} </h2>
                        <h3>
                          {symbol}
                          {product.price}
                        </h3>
                        {product.variants ? (
                          <ul className="color-variant">
                            {product.variants.map((vari, i) => (
                              <li className={vari.color} key={i} title={vari.color} onClick={() => this.onClickHandle(vari.images)}></li>
                            ))}
                          </ul>
                        ) : (
                          ""
                        )}
                        <div className="border-product">
                          <h6 className="product-title">{language === "en" ? "product details" : "পণ্যের বিবরণ"}</h6>
                          <p>{product.shortDetails}</p>
                        </div>
                        <div className="product-description border-product">
                          {product.size ? (
                            <div className="size-box">
                              <ul>
                                {product.size.map((size, i) => {
                                  return (
                                    <li key={i}>
                                      <a href="#">{size}</a>
                                    </li>
                                  );
                                })}
                              </ul>
                            </div>
                          ) : (
                            ""
                          )}
                          <h6 className="product-title">{language === "en" ? "quantity" : "পরিমাণ"}</h6>
                          <div className="qty-box">
                            <div className="input-group">
                              <span className="input-group-prepend">
                                <button
                                  type="button"
                                  className="btn quantity-left-minus"
                                  onClick={this.minusQty}
                                  data-type="minus"
                                  data-field=""
                                >
                                  <i className="fa fa-angle-left"></i>
                                </button>
                              </span>
                              <input
                                type="text"
                                name="quantity"
                                value={this.state.quantity}
                                onChange={this.changeQty}
                                className="form-control input-number"
                              />
                              <span className="input-group-prepend">
                                <button
                                  type="button"
                                  className="btn quantity-right-plus"
                                  onClick={this.plusQty}
                                  data-type="plus"
                                  data-field=""
                                >
                                  <i className="fa fa-angle-right"></i>
                                </button>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="product-buttons">
                          <button className="btn btn-solid" onClick={() => onAddToCartClicked(product, this.state.quantity)}>
                            {language === "en" ? "add to cart" : "কার্ট যোগ করুন"}
                          </button>
                          <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`} className="btn btn-solid">
                            {language === "en" ? "view detail" : "বিস্তারিত দেখুন"}
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale,
  symbol: state.data.symbol
});

export default connect(mapStateToProps)(ProductItem);
